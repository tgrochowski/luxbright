<?php get_header(); ?>

<?php if (!is_front_page()) : ?>
 
<div <?php if (!empty(get_field('custom_style'))) : ?> class="<?php echo get_field('custom_style'); ?>" <?php endif; ?>>
    <?php if (get_field('background') == 'background_image') : ?>
    <div class="top-section" style="background:url(<?php the_field('image'); ?>) no-repeat center center; -webkit-background-size: cover; -moz-background-size: cover; -o-background-size: cover; background-size: cover;">
    <?php else : ?>
    <div class="top-section" style="background-color:<?php the_field('color'); ?>;">
    <?php endif; ?>

    <?php if (get_field('type') == 'text') : ?>
        <div class="row">
            <div class="medium-12 columns text-center">
                <?php if (get_field('text_title')): ?>
                    <h1 class="page-title"><?php the_field('text_title'); ?></h1>
                <?php endif; ?>
                <?php if (get_field('text_ingress')): ?>
                    <div class="row">
                        <div class="medium-10 medium-centered columns text-center">
                            <p class="preamble"><?php the_field('text_ingress', false, false); ?></p>
                        </div>
                    </div>
                <?php endif; ?>
            </div>
        </div>
        <div class="row">
            <div class="medium-10 medium-centered columns text-center">
                <div class="row">
                    <div class="medium-6 columns">
                        <?php the_field('left_text'); ?>
                    </div>
                    <div class="medium-6 columns">
                        <?php the_field('right_text'); ?>
                    </div>
                </div>
            </div>
        </div>
    <?php else : ?>
        <div class="row">
            <div class="medium-10 medium-centered columns">
                <?php if (get_field('video_title')): ?>
                    <h1 class="page-title"><?php the_field('video_title'); ?></h1>
                <?php endif; ?>
                <div class="row">
                    <div class="medium-6 columns">
                        <?php the_field('video_text'); ?>
                    </div>
                    <div class="medium-6 columns">
                        <a data-open="content-4-modal">
                            <?php $image = get_field('video_thumb'); ?>
                            <?php
                            if (!empty($image)):
                                $alt = $image['alt'];
                                $size = 'large';
                                $thumb = $image['sizes'][$size];
                                $width = $image['sizes'][$size . '-width'];
                                $height = $image['sizes'][$size . '-height'];
                                ?>

                                <img src="<?php echo $thumb; ?>" alt="<?php echo $alt; ?>" width="<?php echo $width; ?>" height="<?php echo $height; ?>" />
                            <?php endif; ?>
                        </a>
                        <div class="full reveal" id="content-4-modal" data-reveal>
                            <div class="flex-video">
                                <?php the_field('video_url'); ?>
                            </div>
                            <button class="close-button" data-close aria-label="Close modal" type="button">
                                <span aria-hidden="true">&times;</span>
                            </button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    <?php endif; ?>
    </div>
    </div>
<?php endif; ?>

<?php if (have_rows('sections')) : ?>
    <?php while (have_rows('sections')) : the_row(); ?>
        <?php if (!@include( locate_template('template-parts/' . get_row_layout() . '.php') )) ; ?>
    <?php endwhile; ?>
<?php endif; ?>

    <?php get_footer(); ?>