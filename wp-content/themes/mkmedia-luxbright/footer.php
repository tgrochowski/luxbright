</div>
<footer id="colophon" class="site-footer">
    <div class="footer-top">
        <div class="row">
            <div class="small-12 columns">
                <a href="<?php echo home_url('/'); ?>" title="<?php echo esc_attr(get_bloginfo('name', 'display')); ?>" class="footer-logo" rel="home">
                    <img src="<?php echo get_stylesheet_directory_uri(); ?>/img/logo.png" alt="<?php bloginfo('name'); ?>" width="223" height="42">
                </a>
            </div>
        </div>
        <div class="social">
            <div class="row">
                <div class="medium-4 columns footer-header">
                    <div class="phone">
                        <a href="tel:000 000 000">0710 000 00 00</a>
                    </div>
                </div>
                <div class="medium-4 columns footer-header">
                    <div class="email">
                        <a href="mailto:info@luxbright.se">info@luxbright.se</a>
                    </div>
                </div>
                <div id="footer-header-fb" class="medium-4 columns footer-header">
                    <div class="footer-social-menu">
                        <?php if (is_active_sidebar('social_icons')) : ?>
                            <?php dynamic_sidebar('social_icons'); ?>
                        <?php endif; ?>
                    </div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="small-12 medium-4 columns">
                <div class="box-1">
                    <h5 class="box-title"><?php the_field('box_1_title', 'option'); ?></h5>
                    <div class="footer-column-content">
                        <?php the_field('box_1_text', 'option'); ?>
                    </div>
                    <nav class="site-navigation navigation-footer">
                    <ul>
                        <li class="special menu-item menu-item-type-post_type menu-item-object-page menu-item-860"><a href="http://luxbright.dev/the-story#the-people">Meet our team</a></li>
                    </ul>
                    </nav>
                    
                    <!--<nav class="site-navigation show-for-large">-->
                        <?php // wp_nav_menu(array('theme_location' => 'footer1', 'container' => false)); ?>
                    <!--</nav>-->
                </div>
            </div>
            <div class="small-12 medium-4 columns">
                <div class="box-2">
                    <h5 class="box-title"><?php the_field('box_2_title', 'option'); ?></h5>
                    <div class="footer-column-content">
                        <?php the_field('box_2_text', 'option'); ?>
                    </div>
                    <nav class="site-navigation navigation-footer">
                    <ul>
                        <li class="special menu-item menu-item-type-post_type menu-item-object-page menu-item-860"><a href="http://luxbright.dev/news">News & press</a></li>
                    </ul>
                    </nav>
                </div>
            </div>
            <div class="small-12 medium-4 columns">
                <div class="box-3">
                    <h5 class="box-title"><?php the_field('box_3_title', 'option'); ?></h5>
                    <div class="footer-column-content">
                        <?php the_field('box_3_text', 'option'); ?>
                    </div>
                </div>
            </div>
        </div>
    </div>
</footer>
</div>
</div>
</div>
</div>

<a href="#" id="scroll-top" class="scroll-top"><img src="<?php echo get_stylesheet_directory_uri(); ?>/img/to-top.png" alt="<?php _e('To top', 'luxbright'); ?>" width="66" height="66"></a>

<div id="cookie-bar" class="cookie-bar animated fadeInUp">
    <div class="row">
        <div class="medium-9 columns">
            <p><i class="fa fa-info-circle"></i><?php _e('We use cookies to improve the user experience on', 'luxbright'); ?> <?php bloginfo('name'); ?>. <a href="http://www.cookielaw.org/the-cookie-law/" target="_blank"><?php _e('Read more about Cookies', 'luxbright'); ?></a></p>
        </div>
        <div class="medium-3 columns">
            <a href="#" id="agree" class="button"><i class="fa fa-check"></i><?php _e('Approve', 'luxbright'); ?></a>
        </div>
    </div>
</div>

<?php wp_footer(); ?>
</body>
</html>
