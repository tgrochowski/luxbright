<?php
if ( post_password_required() ) {
	return;
}
?>

<?php if ( have_comments() ) : ?>
	<div id="comments" class="comments-area">
		<div class="commenttitle">
			<h2 class="comments-title">
				<span><?php _e( 'Kommentarer', 'luxbright' ); ?></span>
				<sup><?php comments_number( '', '', '%' ); ?></sup>
			</h2>
			<a href="#respond" class="button radius small"><?php _e( 'Kommentera', 'luxbright' ); ?></a>
		</div>

		<?php if ( get_comment_pages_count() > 1 && get_option( 'page_comments' ) ) : ?>
			<nav id="comment-nav-below" class="navigation comment-navigation" role="navigation">
				 <?php paginate_comments_links(); ?>
			</nav>
		<?php endif; ?>

		<ul class="comment-list">
			<?php wp_list_comments( 'type=comment&callback=luxbright_comment' ); ?>
		</ul>

		<?php if ( get_comment_pages_count() > 1 && get_option( 'page_comments' ) ) : ?>
			<nav id="comment-nav-below" class="navigation comment-navigation" role="navigation">
				 <?php paginate_comments_links(); ?>
			</nav>
		<?php endif; ?>

		<?php if ( ! comments_open() ) : ?>
			<p class="no-comments"><?php _e( 'Kommentarerna på denna posten är avstängda.', 'luxbright' ); ?></p>
		<?php endif; ?>
	</div>
<?php endif; ?>

<?php comment_form($args, $post_id); ?>