<?php get_header(); ?> 

<div class="section news">
	<div class="row">
		<div class="medium-9 columns">
			<?php if ( have_posts() ) : ?>
				
				<?php while ( have_posts() ) : the_post(); ?>

					<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
						<div class="post-date text-center">
							<span class="day"><?php the_time( 'd' ); ?></span>
							<span class="month"><?php the_time( 'M' ); ?></span>
							<span class="year"><?php the_time( 'Y' ); ?></span>
						</div>
						<div class="whole-post">
							<h1><?php the_title(); ?></h1>
							<ul class="meta-info">
								<li class="time"><i class="fa fa-clock-o"></i> <?php the_time( 'H:i' ); ?></li>
								<?php if ( comments_open() ) : ?>
									<li class="comments"><i class="fa fa-comments-o"></i> <?php comments_number( 'Inga kommentarer', 'En kommentar', '% Kommentarer' ); ?></li>
								<?php endif; ?>
								<li class="categories"><i class="fa fa-bookmark"></i><?php the_category(', ') ?></li>
								<?php if ( has_tag() ) : ?>
									<li class="tags"><i class="fa fa-tags"></i><?php the_tags('', ', ', ''); ?></li>
								<?php endif; ?>
							</ul>
							<?php the_content(); ?>
							<?php comments_template(); ?>
						</div>
					</article>

				<?php endwhile; ?>

				<div class="post-navigation row">
					<div class="small-6 medium-6 large-6 columns left">
						<p><?php previous_post_link('%link', '<i>«</i> %title'); ?></p>
					</div>
					<div class="small-6 medium-6 large-6 columns right">
						<p><?php next_post_link('%link', '%title <i>»</i>'); ?></p>
					</div>
				</div>

			<?php endif; ?>
		</div>
		<div class="medium-3 columns">
			<h3><?php _e( 'Arkiv', 'luxbright' ); ?></h3>
			<ul class="sidebar-archive">
				<?php wp_get_archives( array( 'type' => 'monthly', 'limit' => 12 ) ); ?>
			</ul>
			<h3><?php _e( 'Kategorier', 'luxbright' ); ?></h3>
			<ul class="sidebar-categories">
				<?php wp_list_categories('title_li='); ?>
			</ul>
			<h3><?php _e( 'Taggar', 'luxbright' ); ?></h3>
			<div class="sidebar-tags">
				<?php $args = array(
					'smallest'                  => 8, 
					'largest'                   => 22,
					'unit'                      => 'pt', 
					'number'                    => 45,  
					'format'                    => 'flat',
					'separator'                 => ", ",
				); ?>
				<?php wp_tag_cloud( $args ); ?>
			</div>
		</div>
	</div>
</div>

<?php get_footer(); ?>