(function(window, document, $, undefined) {
	"use strict";

	var win = $(window),
		doc = $(document);

	// var baloon = $('.play-button');
	//    function runIt() {
	//        baloon.animate({top:'+=20'}, 1000);
	//        baloon.animate({top:'-=20'}, 1000, runIt);
	//    }
	// runIt();

	/**
	 * Vertical centering for section content
	 */
	function verticalCenter() {
		var content = $('.center-vertical');

		if(win.width() > 640) {
			content.each(function() {
				var that = $(this),
					height = that.outerHeight(),
					parent = that.parent(".row"),
					img = parent.find('img');

				if(img.height() > height) {
					that.css({'margin-top': (parent.height() - height)/2 }).show();
				} else {
					that.show();
					img.css({'margin-top': (parent.height() - img.height())/2 });
				}
			});
		} else {
			content.css({'margin-top': 'auto'}).fadeIn('fast');
		}
	}

	/**
	 * Cookie box
	 */
	function cookieBox() {
		if ($.cookie('medium') === undefined) {
			var container = $('#cookie-bar');

			container.addClass('cookie-not-agreed');

			$('#agree').on('click', function() {
				$.cookie('medium', 'registered', { expires: 365, path: '/' });
				container.removeClass('cookie-not-agreed');
				return false;
			});
			container.on('click', '.button', function() {
				$.cookie('medium', 'redirect');
			});
	    }
	}

	/**
	 * Animate scroll to section #hash-container
	 */
	function slideToSection() {
		var target;

		win.on('scroll');

		$('#hash-navigation').on('click', 'a', function() {
			target = $(this.hash);

			if (target.length !== 0) {
				$('body,html').stop().animate({scrollTop: $(this).scrollTop() + target.position().top - 129}, 500, 'swing');
			}
			return false;
		});
	}

	/**
	 * Animate scroll to section #menu-main-navigation-1
	 */
	function slideToSectionMainNavigation() {
		var target;

		win.on('scroll');

		$('#menu-main-navigation-1 li ul li').on('click', 'a', function() {
			target = $(this.hash);

			if (window.location.href.split('#')[0] === $(this).attr('href').split('#')[0]) {
				if (target.length !== 0) {
					$('body,html').stop().animate({scrollTop: $(this).scrollTop() + target.position().top - 129}, 500, 'swing');
					window.location.hash = '';
					history.pushState('', document.title, window.location.pathname);
				}
				return false;
			}
		});
	}

	/**
	 * Fixed footer
	 */
	function footerOverlap() {
		if($(window).width() > 640) {
			var content = $('#colophon');
			content.each(function() {
				var that = $(this),
					height = that.outerHeight();
					$('#main').css({'margin-bottom': ( height) }).show();
			});
		}
	}

	function menuscroll() {
		var header = $(".site-header");
		$(window).scroll(function() {
		    var scroll = $(window).scrollTop();

		    if (scroll >= 100) {
		        header.removeClass("no-fade").addClass("faded");
		    } else {
		        header.removeClass("faded").addClass("no-fade");
		    }
		});
	}

	function sliderStyles() {
		var elems = $('#working-slider').find('li');
		var maxHeight = -1;
		var directionNav;

		elems.each(function() {
			var h = $(this).height();
			maxHeight = h > maxHeight ? h : maxHeight;
		});

		elems.css("padding-top", $('#section-upper-inner').height()+180);
		elems.css("padding-bottom", 90);
		elems.height(maxHeight);

		$(window).load(function() {
			directionNav = $('#working-slider').find('.flex-direction-nav');
			directionNav.css("bottom", (maxHeight/2)+180);
			if($(window).width() > 640) {
				directionNav.show();
			}
		});
		$(window).resize(function() {
			elems.css("padding-top", $('#section-upper-inner').height()+180);
			elems.css("padding-bottom", 90);
			elems.height(maxHeight);

			if($(window).width() > 640) {
				directionNav.show();
			} else {
				directionNav.hide();
			}
		});
	}

	doc.ready(function() {
		/**
		 * Inizialize Foundation JavaScript
		 */
		doc.foundation();

		sliderStyles();

		/**
		 * Slideshow
		 */
		$('#slideshow').flexslider({
			prevText: '',
			nextText: '',
			directionNav: false,
			pauseOnAction: false,
			start: function(slider) {
				slider.css('visibility','visible').hide().fadeIn('slow');
				if($(window).width() > 640) {
					if(!Modernizr.csstransitions) {
						slider.find('.caption').css({'opacity':1});
					} else {
						slider.find('.flex-active-slide .caption').addClass('animated fadeInUp');
					}
				}
			},
			before: function(slider){
				if($(window).width() > 640) {
					if(!Modernizr.csstransitions) {
						slider.find('.caption').css({'opacity':0});
					} else {
						slider.find('.caption').removeClass('animated fadeInUp');
					}
				}
			},
			after: function(slider) {
				if($(window).width() > 640) {
					if(!Modernizr.csstransitions) {
						slider.find('.flex-active-slide .caption').css({'opacity':1});
					} else {
						slider.find('.flex-active-slide .caption').addClass('animated fadeInUp');
					}
				}
			}
		});

		/**
		 * Scroll to top
		 */
	    $('#scroll-top').on('click', function(){
	        $('html, body').stop().animate({ scrollTop: 0 }, 1000);
	        return false;
	    });

		$('.flexslider-controls').flexslider({
			namespace: "section-",
			slideshow: true,
			prevText: '',
			nextText: '',
			directionNav: false,
			pauseOnAction: false,
			start: function(slider) {
				slider.css('visibility','visible').hide().fadeIn('slow');
			}
		});

		/**
		 * Working slider
		 */
		$('#working-slider').flexslider({
			pauseOnAction: false,
			animation: "fade",
			slideshow: false,
			smoothHeight: false,
			controlNav: true,
			directionNav: true,
			manualControls: ".working-control-nav li",
			useCSS: false,
			start: function(slider){
				slider.css('visibility','visible').removeClass('active');
			}
		});

		/**
		 * Split slider
		 */
		$('#split-slider, #product-split').flexslider({
			pauseOnAction: false,
			animation: "slide",
			slideshow: false,
			smoothHeight: false,
			controlNav: true,
			directionNav: false,
			manualControls: ".split-control-nav li",
			useCSS: false,
			start: function(slider){
				slider.css('visibility','visible').removeClass('active');
			}
		});

		/**
		 * News slider
		 */
		$('#news-slider').flexslider({
			controlNav: false,
			pauseOnAction: false,
			pauseOnHover: true,
			start: function(slider){
				slider.css('visibility','visible').removeClass('active');
			}
		});

		/**
		 * Toggle sub menu in off canvas
		 */
		$('.expand').on('click', function() {
			$(this).next('.sub-menu').slideToggle('fast');
			var that = $(this);
	        that.toggleClass('open');
		});

		$(window).scroll(function() {
		    $('#main > div.content-18').each(function(){
		        if ($(this).is(':in-viewport( 400 )')) {
		        	if($(this).find('video')[0].currentTime === 0) {
		            	$(this).find('video')[0].play();
		        	}
		        }
		    });
		});

		window.sr = ScrollReveal().reveal('.foo', { delay: 3000, scale: 0.9, mobile: false });

	});

	doc.ready(function() {
		slideToSection();
		slideToSectionMainNavigation();
	});

	win.load(function() {
		// slideshowFunc();
		verticalCenter();
		cookieBox();
		footerOverlap();
		menuscroll();
	});
	win.resize(function() {
		// slideshowFunc();
		verticalCenter();
		footerOverlap();
	});

	/**
	 * Placeholder Ie9 fallback
	 */
	if ( !("placeholder" in document.createElement("input")) ) {
		$("input[placeholder], textarea[placeholder]").each(function() {
			var val = $(this).attr("placeholder");
			if ( this.value === "" ) {
				this.value = val;
			}
			$(this).focus(function() {
				if ( this.value === val ) {
					this.value = "";
				}
			}).blur(function() {
				if ( $.trim(this.value) === "" ) {
					this.value = val;
				}
			});
		});

		// Clear default placeholder values on form submit
		$('form').submit(function() {
	        $(this).find("input[placeholder], textarea[placeholder]").each(function() {
	            if ( this.value === $(this).attr("placeholder") ) {
	                this.value = "";
	            }
	        });
	    });
	}

}(window, document, jQuery));
