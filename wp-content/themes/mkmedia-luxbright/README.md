# Luxbright Theme

This is a template to start your own project that uses Foundation 6, Gulp, Bourbon.

## Requirements

To use this template, your computer needs:

* [NodeJS](https://nodejs.org/en/) (0.10 or greater)
* [Gulp](http://gulpjs.com/): Run `[sudo] npm install --global gulp`
* [Bower](http://bower.io): Run `[sudo] npm install -g bower`

## Quickstart

```bash
git clone git@bitbucket.org:mkmedia/luxbright-theme.git
npm install && bower install
```

Finally, run `npm start` to run the Sass compiler. It will re-run every time you save a Sass file.