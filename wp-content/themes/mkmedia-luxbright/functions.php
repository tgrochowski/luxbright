<?php
/**
 * Sets up theme defaults and registers support for various WordPress features.
 */
if ( ! function_exists( 'luxbright_setup' ) ) :
	function luxbright_setup() {

		/**
		 * Make theme available for translation
		 */
		// load_theme_textdomain( 'luxbright', get_template_directory() . '/languages' );

		/**
		 * Enables post and comment RSS feed links to head
		 */
		add_theme_support( 'automatic-feed-links' );

		/**  
		 * Register thumbnails
		 */
		add_theme_support( 'post-thumbnails' );
		set_post_thumbnail_size( 970, 410, true );
		add_image_size( 'top_image', 970, 410, true );
		add_image_size( 'slideshow', 1480, 517, true );
		add_image_size( 'member_thumb', 200, 200, true );
		add_image_size( 'news_image', 600, 364, true );
		add_image_size( 'slider_icon', 131, 83, true );

		/**
		 * Register menus
		 */
		register_nav_menus(
			array(
				'primary' => 'Main navigation',
			),
                        array(
				'footer1' => 'Meet Team',
			),
                        array(
				'footer2' => 'News Press',
			)
		);
	}
endif;
add_action( 'after_setup_theme', 'luxbright_setup' );

/** 
 * ACF filter for speeding up javascript
 */
add_filter('acf/compatibility/field_wrapper_class', '__return_false');

/** 
 * Google API
 */
function my_acf_init() {
	
	acf_update_setting('google_api_key', 'AIzaSyAYW6QwmhwFnrngiBVrArNZpE_qU3_dK_A');
}

add_action('acf/init', 'my_acf_init');

/** 
 * Footer
 */
if( function_exists('acf_add_options_page') ) {
	
	acf_add_options_page(array(
		'page_title' 	=> 'Footer',
		'menu_title'	=> 'Footer',
	));
	
}

/** 
 * Register custom post types.
 */
function luxbright_post_type_register() {

	// Business cases
	$args = array(
		'label' => __( 'Business cases', 'luxbright' ),
		'public' => true,
		'publicly_queryable' => true,
		'show_ui' => true,
		'show_in_menu' => true,
		'query_var' => true,
		'rewrite' => array( 'slug' => 'business-cases' ),
		'capability_type' => 'post',
		'has_archive' => true,
		'hierarchical' => false,
		'menu_position' => null,
		'menu_icon' => 'dashicons-welcome-widgets-menus',
		'supports' => array( 'title', 'editor', 'thumbnail' )
	);
	register_post_type( 'business_cases', $args );

	// Slideshow
	$args = array(
		'label' => __( 'Bildspel', 'luxbright' ),
		'public' => true,
		'publicly_queryable' => true,
		'show_ui' => true, 
		'show_in_menu' => true, 
		'query_var' => true,
		'rewrite' => array( 'slug' => 'bildspel' ),
		'capability_type' => 'post',
		'has_archive' => true, 
		'hierarchical' => false,
		'menu_position' => null,
		'menu_icon' => 'dashicons-slides',
		'supports' => array( 'title', 'editor', 'thumbnail' )
	); 
	register_post_type( 'slideshow', $args );

	// Events
	$args = array(
		'label' => __( 'Events', 'luxbright' ),
		'public' => true,
		'publicly_queryable' => true,
		'show_ui' => true, 
		'show_in_menu' => true, 
		'query_var' => true,
		'capability_type' => 'post',
		'has_archive' => true,
		'hierarchical' => false,
		'menu_position' => null,
		'menu_icon' => 'dashicons-calendar',
		'supports' => array( 'title', 'editor' )
	); 
	register_post_type('events', $args);
}
add_action( 'init', 'luxbright_post_type_register' );

/**
 * Enqueues scripts for front end.
 */
function luxbright_enqueue_scripts() {
	if ( !is_admin() ) :
		wp_deregister_script( 'jQuery' );
		// wp_register_script( 'google-api', 'https://maps.googleapis.com/maps/api/js?v=3.exp&sensor=false', '', '', true );
		wp_register_script( 'app', get_template_directory_uri() . '/js/dist/app.min.js', '', '', true );

		// wp_enqueue_script( 'google-api' );
		wp_enqueue_script( 'app' );
	endif;

	// if ( is_front_page() ) :
	// 	wp_deregister_script( 'google-api' );
	// endif;
}
add_action( 'wp_enqueue_scripts', 'luxbright_enqueue_scripts', 1 );

/**
 * Disable contact form 7 css & js on Frontpage
 */
add_action( 'wp_print_scripts', 'deregister_cf7_javascript', 100 );
function deregister_cf7_javascript() {
	if ( is_front_page() ) {
		wp_deregister_script( 'contact-form-7' );
	}
}

add_action( 'wp_print_styles', 'deregister_cf7_styles', 100 );
function deregister_cf7_styles() {
	if ( is_front_page() ) {
		wp_deregister_style( 'contact-form-7' );
	}
}

/** 
 * Custom sort posts for event post type
 */
function set_custom_post_types_admin_order($wp_query) {
  if (is_admin()) {

	// Get the post type from the query
	$post_type = $wp_query->query['post_type'];

	if ( $post_type == 'events') {

	  // 'orderby' value can be any column name
	  $wp_query->set('orderby', 'event_start_date');

	  // 'order' value can be ASC or DESC
	  $wp_query->set('order', 'DESC');
	}
  }
}
add_filter('pre_get_posts', 'set_custom_post_types_admin_order');

/** 
 * Custom admin columns for events post type
 */
function my_custom_columns($columns) {
	$columns = array(
		'cb' 				=> '<input type="checkbox" />',
		'title' 			=> 'Event',
		'date'				=> 'Publicated',
		'event_start_date' 	=> 'Date',
		'event_duration' 	=> 'Days',
		'event_location' 	=> 'Location'
	);
	return $columns;
}

function my_custom_eve_columns($column) {
	global $post;

	$startDate = get_field( 'event_start_date' );
	$duration = get_field( 'event_duration' );
	$location = get_field( 'event_location' );

	if ( $column == 'event_start_date' ) {
		echo $startDate;
	}
	elseif ( $column == 'event_duration' ) {
		echo $duration;
	}
	elseif ( $column == 'event_location' ) {
		echo $location;
	}
}
add_filter("manage_edit-event_columns", "my_custom_columns");
add_action("manage_posts_custom_column", "my_custom_eve_columns");

/** 
 * Get event date
 */
function event_date() {

	date_default_timezone_set( get_option( 'timezone_string' ) );

	$sDate = get_field( 'event_start_date' );
	$sDuration = get_field( 'event_duration' );

	if ( $sDuration == 1 ) {
		$days = date_i18n( 'j', strtotime( $sDate ) ) . '/' . date_i18n( 'n', strtotime( $sDate ) );
	}
	else {
		$days = date_i18n( 'j', strtotime( $sDate ) ) . '-' . date_i18n( 'j', strtotime( $sDate . ' + ' . ( $sDuration - 1 ) . ' days' ) );
		
		if ( date_i18n( 'M', strtotime( $sDate ) ) != date_i18n( 'M', strtotime( $sDate . ' + ' . ( $sDuration - 1 ) . ' days') ) ) {
			$days .= date_i18n( 'F', strtotime( $sDate ) ) . '-' . date_i18n( 'F ', strtotime( $sDate . ' + ' . ( $sDuration - 1 ) . ' days' ) ) . date_i18n( 'Y', strtotime( $sDate ) );
		} else {
			$days .= '/' . date_i18n( 'n', strtotime( $sDate ) );
		}
	}
	$days;

	return $days;
}

/**
 * Top Bar Walker
 *
 * @since 1.0.0
 */
class Top_Bar_Walker extends Walker_Nav_Menu {
	/**
	 * @see Walker_Nav_Menu::start_lvl()
	 * @since 1.0.0
	 *
	 * @param string $output Passed by reference. Used to append additional content.
	 * @param int $depth Depth of page. Used for padding.
	 */
	function start_lvl( &$output, $depth = 0, $args = array() ) {
		$output .= "\n<ul class=\"sub-menu dropdown\">\n";
	}

	/**
	 * @see Walker_Nav_Menu::start_el()
	 * @since 1.0.0
	 *
	 * @param string $output Passed by reference. Used to append additional content.
	 * @param object $item Menu item data object.
	 * @param int $depth Depth of menu item. Used for padding.
	 * @param object $args
	 */

	function start_el( &$output, $object, $depth = 0, $args = array(), $current_object_id = 0 ) {
		$item_html = '';
		parent::start_el( $item_html, $object, $depth, $args );  

		$classes = empty( $object->classes ) ? array() : ( array ) $object->classes;  

		if ( in_array('label', $classes) ) {
			$item_html = preg_replace( '/<a[^>]*>( .* )<\/a>/iU', '<label>$1</label>', $item_html );
		}

		if ( in_array('divider', $classes) ) {
			$item_html = preg_replace( '/<a[^>]*>( .* )<\/a>/iU', '', $item_html );
		}

		$output .= $item_html;
	}

	/**
	 * @see Walker::display_element()
	 * @since 1.0.0
	 *
	 * @param object $element Data object
	 * @param array $children_elements List of elements to continue traversing.
	 * @param int $max_depth Max depth to traverse.
	 * @param int $depth Depth of current element.
	 * @param array $args
	 * @param string $output Passed by reference. Used to append additional content.
	 * @return null Null on failure with no changes to parameters.
	 */
	function display_element( $element, &$children_elements, $max_depth, $depth=0, $args, &$output ) {
		$element->has_children = !empty( $children_elements[$element->ID] );
		$element->classes[] = ( $element->current || $element->current_item_ancestor ) ? 'active' : '';
		$element->classes[] = ( $element->has_children ) ? 'has-dropdown' : '';

		parent::display_element( $element, $children_elements, $max_depth, $depth, $args, $output );
	}
}

/** 
 * Add class to nav menu if li-item has sub menu
 */
class Off_Canvas_Walker extends Walker_Nav_Menu {
	function start_lvl( &$output, $depth = 0, $args = array() ) {
		$indent = str_repeat("\t", $depth);
		$output = preg_replace( "/(.*)(\<li.*?class\=\")([^\"]*)(\".*?)$/", "$1$2$3 menu-anchor$4", $output );
		$output .= '<span class="expand"><i class="fa fa-chevron-right"></i></span>';
		$output .= $indent . '<ul class="sub-menu">';
	}
}

/** 
 * Custom excerpt length
 */
function luxbright_custom_excerpt_length( $length ) {
	return 18;
}
add_filter( 'excerpt_length', 'luxbright_custom_excerpt_length', 999 );

/** 
 * Custom excerpt more
 */
function new_excerpt_more($more) {
	global $post;
	if ($post->post_type != 'events')
	{
		$output .= '...';  
	}
	return $output;
}
add_filter('excerpt_more', 'new_excerpt_more');

/** 
 * Pagination
 */
function foundation_pagination($arrows = true, $ends = true, $pages = 2) {
	if (is_singular()) return;

	global $wp_query, $paged;
	$pagination = '';

	$max_page = $wp_query->max_num_pages;
	if ($max_page == 1) return;
	if (empty($paged)) $paged = 1;

	if ($arrows) $pagination .= foundation_pagination_link($paged - 1, 'arrow' . (($paged <= 1) ? ' unavailable' : ''), '&laquo;', 'Previous Page');
	if ($ends && $paged > $pages + 1) $pagination .= foundation_pagination_link(1);
	if ($ends && $paged > $pages + 2) $pagination .= foundation_pagination_link(1, 'unavailable', '&hellip;');
	for ($i = $paged - $pages; $i <= $paged + $pages; $i++) {
		if ($i > 0 && $i <= $max_page)
			$pagination .= foundation_pagination_link($i, ($i == $paged) ? 'current' : '');
	}
	if ($ends && $paged < $max_page - $pages - 1) $pagination .= foundation_pagination_link($max_page, 'unavailable', '&hellip;');
	if ($ends && $paged < $max_page - $pages) $pagination .= foundation_pagination_link($max_page);

	if ($arrows) $pagination .= foundation_pagination_link($paged + 1, 'arrow' . (($paged >= $max_page) ? ' unavailable' : ''), '&raquo;', 'Next Page');

	$pagination = '<ul class="pagination">' . $pagination . '</ul>';
	$pagination = '<div class="pagination-centered">' . $pagination . '</div>';

	echo $pagination;
}
 
function foundation_pagination_link($page, $class = '', $content = '', $title = '') {
	$id = sanitize_title_with_dashes('pagination-page-' . $page . ' ' . $class);
	$href = (strrpos($class, 'unavailable') === false && strrpos($class, 'current') === false) ? get_pagenum_link($page) : "#$id";

	$class = empty($class) ? $class : " class=\"$class\"";
	$content = !empty($content) ? $content : $page;
	$title = !empty($title) ? $title : 'Page ' . $page;

	return "<li$class><a id=\"$id\" href=\"$href\" title=\"$title\">$content</a></li>\n";
}

/**
 *  Get sub menu items for page based on acf sections
 */
function luxbright_get_sub_menu_items() {
	global $post;
	$menu_items = array();

	$sections = get_field('sections');

	foreach( $sections as $section ) {
		if ( $section['acf_fc_layout'] == 'content_1' ) {
			array_push( $menu_items, $section['content_1_menu_item'] );
		}
		if ( $section['acf_fc_layout'] == 'content_2' ) {
			array_push( $menu_items, $section['content_2_menu_item'] );
		}
		if ( $section['acf_fc_layout'] == 'content_3' ) {
			array_push( $menu_items, $section['content_3_menu_item'] );
		}
		if ( $section['acf_fc_layout'] == 'content_4' ) {
			array_push( $menu_items, $section['content_4_menu_item'] );
		}
		if ( $section['acf_fc_layout'] == 'content_5' ) {
			array_push( $menu_items, $section['content_5_menu_item'] );
		}
		if ( $section['acf_fc_layout'] == 'content_6' ) {
			array_push( $menu_items, $section['content_6_menu_item'] );
		}
		if ( $section['acf_fc_layout'] == 'content_7' ) {
			array_push( $menu_items, $section['content_7_menu_item'] );
		}
		if ( $section['acf_fc_layout'] == 'content_8' ) {
			array_push( $menu_items, $section['content_8_menu_item'] );
		}
		if ( $section['acf_fc_layout'] == 'content_9' ) {
			array_push( $menu_items, $section['content_9_menu_item'] );
		}
		if ( $section['acf_fc_layout'] == 'content_10' ) {
			array_push( $menu_items, $section['content_10_menu_item'] );
		}
		if ( $section['acf_fc_layout'] == 'content_11' ) {
			array_push( $menu_items, $section['content_11_menu_item'] );
		}
		if ( $section['acf_fc_layout'] == 'content_12' ) {
			array_push( $menu_items, $section['content_12_menu_item'] );
		}
		if ( $section['acf_fc_layout'] == 'content_13' ) {
			array_push( $menu_items, $section['content_13_menu_item'] );
		}
		if ( $section['acf_fc_layout'] == 'content_14' ) {
			array_push( $menu_items, $section['content_14_menu_item'] );
		}
		if ( $section['acf_fc_layout'] == 'content_15' ) {
			array_push( $menu_items, $section['content_15_menu_item'] );
		}
		if ( $section['acf_fc_layout'] == 'content_16' ) {
			array_push( $menu_items, $section['content_16_menu_item'] );
		}
		if ( $section['acf_fc_layout'] == 'content_18' ) {
			array_push( $menu_items, $section['content_18_menu_item'] );
		}
              if ( $section['acf_fc_layout'] == 'properties_grid' ) {
                  
			array_push( $menu_items, $section['properties_grid_menu_item'] );
		}
	}

	return $menu_items;
}

/**
 * Generate section id
 */
function luxbright_create_section_id( $str ) {
	$str = sanitize_title( $str );
	return $str;
}

/** 
 * Changed admin logo
 */
function admin_logo() { ?>
    <style type="text/css">
        body.login div#login h1 a {
            background-image: url(<?php echo get_stylesheet_directory_uri(); ?>/img/logo.png);
            width: 167px;
			height: 39px;
			margin: 0 auto;
			margin-bottom: 30px;
			background-size: 167px;
        }
    </style>
<?php }
add_action( 'login_enqueue_scripts', 'admin_logo' );

/** 
 * Stop hackers from finding username
 */
function author_archive_redirect() {
  if( is_author() ) {
      wp_redirect( home_url(), 301 );
      exit;
  }
}
add_action( 'template_redirect', 'author_archive_redirect' );

/** 
 * Remove whitespace before title
 */
function luxbright_titledespacer($title) {
	return trim($title);
}
add_filter('wp_title', 'luxbright_titledespacer');

/** 
 * Browser detection.
 */
function luxbright_browser_body_class() {
	global $is_lynx, $is_gecko, $is_IE, $is_opera, $is_NS4, $is_safari, $is_chrome, $is_iphone;
	$classes = array();
	
	if ( $is_lynx ) $classes[] = 'lynx';
	elseif ( $is_gecko ) $classes[] = 'gecko';
	elseif ( $is_opera ) $classes[] = 'opera';
	elseif ( $is_NS4 ) $classes[] = 'ns4';
	elseif ( $is_safari ) $classes[] = 'safari';
	elseif ( $is_chrome ) $classes[] = 'chrome';
	elseif ( $is_IE ) {
			$classes[] = 'ie';
			if(preg_match('/MSIE ([0-9]+)([a-zA-Z0-9.]+)/', $_SERVER['HTTP_USER_AGENT'], $browser_version))
			$classes[] = 'ie'.$browser_version[1];
	} else $classes[] = 'unknown';
	
	if ( $is_iphone ) $classes[] = 'iphone';
	
	if ( stristr( $_SERVER['HTTP_USER_AGENT'], "mac" ) ) {
		$classes[] = 'osx';
	} elseif ( stristr( $_SERVER['HTTP_USER_AGENT'], "linux" ) ) {
		$classes[] = 'linux';
	} elseif ( stristr( $_SERVER['HTTP_USER_AGENT'], "windows" ) ) {
		$classes[] = 'windows';
	}
	return $classes;
}

function luxbright_widgets_init() {

	register_sidebar( array(
		'name'          => 'Social Icons',
		'id'            => 'social_icons',
		'before_widget' => '<div class="simple-social-icons">',
		'after_widget'  => '</div>',
		// 'before_title'  => '<h2 class="rounded">',
		// 'after_title'   => '</h2>',
	) );

}
add_action( 'widgets_init', 'luxbright_widgets_init' );