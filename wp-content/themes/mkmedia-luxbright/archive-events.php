<?php get_header(); ?>  

<div class="section archive-events">
	<div class="row">
		<div class="medium-9 medium-centered columns">
			<h1 class="page-title"><?php the_archive_title(); ?></h1>
			<?php if ( have_posts() ) : ?>
				
				<?php while ( have_posts() ) : the_post(); ?>

					<article id="event-<?php the_ID(); ?>">
						<h1 class="event-title"><?php the_title(); ?></h1>
						<div class="meta-info">
							<span class="date"><i class="fa fa-calendar-o"></i> <?php echo event_date(); ?></span> 
							<span class="location"><i class="fa fa-location-arrow"></i> <?php the_field('event_location'); ?></span>
						</div>
						<?php the_excerpt(); ?>
						<a href="<?php the_permalink(); ?>" class="go-to">Go to event</a>
					</article>
				<?php endwhile; ?>

				<?php foundation_pagination(); ?>

			<?php endif; ?>
		</div>
	</div>
</div>

<?php get_footer(); ?>