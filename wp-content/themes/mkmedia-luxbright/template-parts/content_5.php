<?php
/**
 * Content-5
 * Small image left section
 */ ?>
<?php $menu_item = get_sub_field( 'content_5_menu_item' ); ?>

<div id="<?php echo luxbright_create_section_id( $menu_item ); ?>" class="section content-5"<?php if ( get_sub_field( 'background_color' ) ): ?> style="background-color:<?php the_sub_field( 'background_color' ); ?>;"<?php endif; ?>>
	<div class="row">
		<div class="large-12 columns">
			<?php if ( have_rows( 'repeater_field' ) ) : ?>
				<div class="row small-up-1 medium-up-2">
					<?php while ( have_rows( 'repeater_field' ) ) : the_row(); ?>
						<div class="column">
							<div class="box">
								<div class="row">
									<div class="medium-4 columns">
										<?php $image = get_sub_field( 'image' ); ?>
										<?php if ( !empty($image) ): 
											
											$alt = $image['alt'];
											$size = 'member_thumb';
											$thumb = $image['sizes'][ $size ];
											$width = $image['sizes'][ $size . '-width' ];
											$height = $image['sizes'][ $size . '-height' ]; ?>

											<img src="<?php echo $thumb; ?>" alt="<?php echo $alt; ?>" width="<?php echo $width; ?>" height="<?php echo $height; ?>" />
										<?php endif; ?>
									</div>
									<div class="medium-8 columns">
										<?php if ( get_sub_field( 'pre_title' ) ): ?>
											<p class="pre-title"><?php the_sub_field( 'pre_title' ); ?></p>
										<?php endif; ?>
										<?php if ( get_sub_field( 'title' ) ): ?>
											<h3 class="title"><?php the_sub_field( 'title' ); ?></h3>
										<?php endif; ?>
										<?php the_sub_field( 'text' ); ?>
									</div>
								</div>
							</div>
						</div>
					<?php endwhile; ?>
				</div>
			<?php endif; ?>
		</div>
	</div>
</div>