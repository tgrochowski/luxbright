<?php
/**
 * Content-4
 * Video section
 */ ?>
<?php $menu_item = get_sub_field( 'content_4_menu_item' ); ?>

<?php if ( get_sub_field( 'type' ) == 'background_image' ) : ?>
	<div id="<?php echo luxbright_create_section_id( $menu_item ); ?>" class="section content-4" style="background:url(<?php the_sub_field( 'image' ); ?>) no-repeat center center; -webkit-background-size: cover; -moz-background-size: cover; -o-background-size: cover; background-size: cover;">
<?php elseif ( get_sub_field( 'type' ) == 'parallax' ) : ?>
	<div id="<?php echo luxbright_create_section_id( $menu_item ); ?>" class="section content-4 parallax-window" data-z-index="2" data-parallax="scroll" data-image-src="<?php the_sub_field( 'parallax_image' ); ?>">
<?php elseif ( get_sub_field( 'type' ) == 'background_color' ) : ?>
	<div id="<?php echo luxbright_create_section_id( $menu_item ); ?>" class="section content-4" style="background-color:<?php the_sub_field( 'color' ); ?>;">
<?php endif; ?>
	<div class="row">
		<div class="medium-10 medium-centered columns">
			<?php if ( get_sub_field( 'title' ) ): ?>
				<h2 class="section-title"><?php the_sub_field( 'title' ); ?></h2>
			<?php endif; ?>
			<div class="row">
				<?php if ( get_sub_field( 'video_align' ) == 'left' ) : ?>
					<div class="medium-6 columns show-for-medium">
						<a data-open="content-4-modal">
							<?php $image = get_sub_field( 'video_thumb' ); ?>
							<?php if ( !empty($image) ): 
								
								$alt = $image['alt'];
								$size = 'large';
								$thumb = $image['sizes'][ $size ];
								$width = $image['sizes'][ $size . '-width' ];
								$height = $image['sizes'][ $size . '-height' ]; ?>

								<img src="<?php echo $thumb; ?>" alt="<?php echo $alt; ?>" width="<?php echo $width; ?>" height="<?php echo $height; ?>" />
							<?php endif; ?>
						</a>
						<div class="full reveal" id="content-4-modal" data-reveal>
							<div class="flex-video">
								<?php the_sub_field('video'); ?>
							</div>
							<button class="close-button" data-close aria-label="Close modal" type="button">
								<span aria-hidden="true">&times;</span>
							</button>
						</div>
					</div>
					<div class="medium-6 columns">
						<?php if ( get_sub_field( 'pre_title' ) ): ?>
							<p class="pre-title"><?php the_sub_field( 'pre_title' ); ?></p>
						<?php endif; ?>
						<?php if ( get_sub_field( 'desc_title' ) ): ?>
							<h3 class="desc-title"><?php the_sub_field( 'desc_title' ); ?></h3>
						<?php endif; ?>
						<?php the_sub_field( 'text' ); ?>
					</div>
					<div class="small-12 columns show-for-small-only">
						<a data-open="content-4-modal">
							<?php $image = get_sub_field( 'video_thumb' ); ?>
							<?php if ( !empty($image) ): 
								
								$alt = $image['alt'];
								$size = 'large';
								$thumb = $image['sizes'][ $size ];
								$width = $image['sizes'][ $size . '-width' ];
								$height = $image['sizes'][ $size . '-height' ]; ?>

								<img src="<?php echo $thumb; ?>" alt="<?php echo $alt; ?>" width="<?php echo $width; ?>" height="<?php echo $height; ?>" />
							<?php endif; ?>
						</a>
					</div>
				<?php else : ?>
					<div class="medium-6 columns">
						<?php if ( get_sub_field( 'pre_title' ) ): ?>
							<p class="pre-title"><?php the_sub_field( 'pre_title' ); ?></p>
						<?php endif; ?>
						<?php if ( get_sub_field( 'desc_title' ) ): ?>
							<h3 class="desc-title"><?php the_sub_field( 'desc_title' ); ?></h3>
						<?php endif; ?>
						<?php the_sub_field( 'text' ); ?>
					</div>
					<div class="medium-6 columns">
						<a data-open="content-4-modal">
							<?php $image = get_sub_field( 'video_thumb' ); ?>
							<?php if ( !empty($image) ): 
								
								$alt = $image['alt'];
								$size = 'large';
								$thumb = $image['sizes'][ $size ];
								$width = $image['sizes'][ $size . '-width' ];
								$height = $image['sizes'][ $size . '-height' ]; ?>

								<img src="<?php echo $thumb; ?>" alt="<?php echo $alt; ?>" width="<?php echo $width; ?>" height="<?php echo $height; ?>" />
							<?php endif; ?>
						</a>
						<div class="full reveal" id="content-4-modal" data-reveal>
							<div class="flex-video">
								<?php the_sub_field('video'); ?>
							</div>
							<button class="close-button" data-close aria-label="Close modal" type="button">
								<span aria-hidden="true">&times;</span>
							</button>
						</div>
					</div>
				<?php endif; ?>
			</div>
		</div>
	</div>
</div>