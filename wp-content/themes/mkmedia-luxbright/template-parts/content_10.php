<?php
/**
 * Content-10
 * Events
 */ ?>
<?php $menu_item = get_sub_field( 'content_10_menu_item' ); ?>

<div id="<?php echo luxbright_create_section_id( $menu_item ); ?>" class="section content-10">
	<div class="row">
		<div class="medium-12 columns text-center">
			<?php if ( get_sub_field( 'title' ) ): ?>
				<h2 class="section-title"><?php the_sub_field( 'title' ); ?></h2>
			<?php endif; ?>
			<?php if ( get_sub_field( 'ingress' ) ): ?>
				<div class="row">
					<div class="medium-10 medium-centered columns text-center">
						<p class="preamble"><?php the_sub_field( 'ingress', false, false ); ?></p>
					</div>
				</div>
			<?php endif; ?>
		</div>
	</div>

	<?php $today = date( 'Ymd' ); ?>

	<?php $latest_event = array(
		'post_type' => 'events',
		'posts_per_page' => -1,
		'meta_key' => 'event_start_date',
		'meta_value' => $today,
		'orderby' => 'meta_value',
		'order' => 'asc',
		'meta_compare' => '>=',
		'paged' => $paged
	); ?>

	<?php query_posts( $latest_event ); ?>
	<?php if (have_posts()) : ?>
		<div class="row small-up-1 medium-up-1 large-up-3" data-equalizer data-equalize-on="medium">
			<?php while (have_posts()) : the_post(); ?>

				<article id="event-<?php the_ID(); ?>" class="column">
					<div class="box" data-equalizer-watch>
						<span class="date"><?php echo event_date(); ?></span>
						<span class="location"><?php the_field('event_location'); ?></span>
						<h2 class="event-title"><?php the_title(); ?></h2>
						<a href="<?php the_permalink(); ?>" class="go-to">Go to event</a>
						<?php the_excerpt(); ?>
					</div>
				</article>

			<?php endwhile; ?>
		</div>
	<?php else : ?>

		<p class="text-center"><b>There are no upcoming events.</b></p>

	<?php endif; wp_reset_query(); ?>
</div>