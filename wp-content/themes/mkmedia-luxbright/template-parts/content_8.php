<?php
/**
 * Content-8
 * Three columns section with image
 */ ?>
<?php $menu_item = get_sub_field( 'content_8_menu_item' ); ?>

<div id="<?php echo luxbright_create_section_id( $menu_item ); ?>" class="section content-8">
	<?php if ( get_sub_field( 'title' ) ): ?>
		<div class="row">
			<div class="large-12 columns">
				<?php if ( get_sub_field( 'title' ) ): ?>
					<h2 class="section-title"><?php the_sub_field( 'title' ); ?></h2>
				<?php endif; ?>
			</div>
		</div>
	<?php endif; ?>
	<div class="row">
		<div class="medium-4 columns">
			<?php if ( get_sub_field( 'text_video_1' ) == 'text' ) : ?>
				<?php the_sub_field( 'text_1' ); ?>
			<?php elseif ( get_sub_field( 'text_video_1' ) == 'image' ) : ?>
				<?php $image = get_sub_field( 'image_1' ); ?>
				<?php if ( !empty($image) ): 
					
					$alt = $image['alt'];
					$size = 'large';
					$thumb = $image['sizes'][ $size ];
					$width = $image['sizes'][ $size . '-width' ];
					$height = $image['sizes'][ $size . '-height' ]; ?>

					<img src="<?php echo $thumb; ?>" alt="<?php echo $alt; ?>" width="<?php echo $width; ?>" height="<?php echo $height; ?>" />
				<?php endif; ?>
			<?php else : ?>
				<a data-open="exampleModal1">
					<?php $image = get_sub_field( 'video_image_1' ); ?>
					<?php if ( !empty($image) ): 
						
						$alt = $image['alt'];
						$size = 'large';
						$thumb = $image['sizes'][ $size ];
						$width = $image['sizes'][ $size . '-width' ];
						$height = $image['sizes'][ $size . '-height' ]; ?>

						<img src="<?php echo $thumb; ?>" alt="<?php echo $alt; ?>" width="<?php echo $width; ?>" height="<?php echo $height; ?>" />
					<?php endif; ?>
				</a>
				<div class="full reveal" id="exampleModal1" data-reveal>
					<div class="flex-video">
						<?php the_sub_field('video_1'); ?>
					</div>
					<button class="close-button" data-close aria-label="Close modal" type="button">
						<span aria-hidden="true">&times;</span>
					</button>
				</div>
			<?php endif; ?>
		</div>
		<div class="medium-4 columns">
			<?php if ( get_sub_field( 'text_video_2' ) == 'text' ) : ?>
				<?php the_sub_field( 'text_2' ); ?>
			<?php elseif ( get_sub_field( 'text_video_2' ) == 'image' ) : ?>
				<?php $image = get_sub_field( 'image_2' ); ?>
				<?php if ( !empty($image) ): 
					
					$alt = $image['alt'];
					$size = 'large';
					$thumb = $image['sizes'][ $size ];
					$width = $image['sizes'][ $size . '-width' ];
					$height = $image['sizes'][ $size . '-height' ]; ?>

					<img src="<?php echo $thumb; ?>" alt="<?php echo $alt; ?>" width="<?php echo $width; ?>" height="<?php echo $height; ?>" />
				<?php endif; ?>
			<?php else : ?>
				<a data-open="exampleModal2">
					<?php $image = get_sub_field( 'video_image_2' ); ?>
					<?php if ( !empty($image) ): 
						
						$alt = $image['alt'];
						$size = 'large';
						$thumb = $image['sizes'][ $size ];
						$width = $image['sizes'][ $size . '-width' ];
						$height = $image['sizes'][ $size . '-height' ]; ?>

						<img src="<?php echo $thumb; ?>" alt="<?php echo $alt; ?>" width="<?php echo $width; ?>" height="<?php echo $height; ?>" />
					<?php endif; ?>
				</a>
				<div class="full reveal" id="exampleModal2" data-reveal>
					<div class="flex-video">
						<?php the_field('video_2'); ?>
					</div>
					<button class="close-button" data-close aria-label="Close modal" type="button">
						<span aria-hidden="true">&times;</span>
					</button>
				</div>
			<?php endif; ?>
		</div>
		<div class="medium-4 columns">
			<?php if ( get_sub_field( 'text_video_3' ) == 'text' ) : ?>
				<?php the_sub_field( 'text_3' ); ?>
			<?php elseif ( get_sub_field( 'text_video_3' ) == 'image' ) : ?>
				<?php $image = get_sub_field( 'video_image_3' ); ?>
				<?php if ( !empty($image) ): 
					
					$alt = $image['alt'];
					$size = 'large';
					$thumb = $image['sizes'][ $size ];
					$width = $image['sizes'][ $size . '-width' ];
					$height = $image['sizes'][ $size . '-height' ]; ?>

					<img src="<?php echo $thumb; ?>" alt="<?php echo $alt; ?>" width="<?php echo $width; ?>" height="<?php echo $height; ?>" />
				<?php endif; ?>
			<?php else : ?>
				<a data-open="exampleModal3">
					<?php $image = get_sub_field( 'video_image_3' ); ?>
					<?php if ( !empty($image) ): 
						
						$alt = $image['alt'];
						$size = 'large';
						$thumb = $image['sizes'][ $size ];
						$width = $image['sizes'][ $size . '-width' ];
						$height = $image['sizes'][ $size . '-height' ]; ?>

						<img src="<?php echo $thumb; ?>" alt="<?php echo $alt; ?>" width="<?php echo $width; ?>" height="<?php echo $height; ?>" />
					<?php endif; ?>
				</a>
				<div class="full reveal" id="exampleModal3" data-reveal>
					<div class="flex-video">
						<?php the_field('video_3'); ?>
					</div>
					<button class="close-button" data-close aria-label="Close modal" type="button">
						<span aria-hidden="true">&times;</span>
					</button>
				</div>
			<?php endif; ?>
		</div>
	</div>
</div>