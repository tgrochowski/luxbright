<?php
/**
 * Content-6
 * Table section
 */ ?>
<?php $menu_item = get_sub_field( 'content_6_menu_item' ); ?>

<div id="<?php echo luxbright_create_section_id( $menu_item ); ?>" class="section content-6"<?php if ( get_sub_field( 'background_color' ) ): ?> style="background-color:<?php the_sub_field( 'background_color' ); ?>;"<?php endif; ?>>
	<?php if ( get_sub_field( 'introduction_on' ) == 'yes' ) : ?>
		<div class="row">
			<div class="medium-10 medium-centered columns">
				<div class="section-introduction text-center">
					<?php if ( get_sub_field( 'title' ) ): ?>
						<h2 class="section-title"><?php the_sub_field( 'title' ); ?></h2>
					<?php endif; ?>
					<?php if ( get_sub_field( 'ingress' ) ) : ?>
						<p class="preamble"><?php the_sub_field( 'ingress', false, false ); ?></p>
					<?php endif; ?>
				</div>
			</div>
		</div>
	<?php endif; ?>
	<div class="row">
		<div class="small-12 columns">
			<table>
				<thead>
					<tr>
						<th class="comparison" width="400">Comparison</th>
						<th class="standard" width="150"><img src="<?php echo get_stylesheet_directory_uri(); ?>/img/standard_xray.png"><span>Standard</span></th>
						<th class="m-series" width="150"><img src="<?php echo get_stylesheet_directory_uri(); ?>/img/m_series.png"><span>M-series</span></th>
						<th class="fm-series" width="150"><img src="<?php echo get_stylesheet_directory_uri(); ?>/img/m_series.png"><span>FM-series</span></th>
					</tr>
				</thead>
				<tbody>
					<?php if ( have_rows( 'repeater_field' ) ) : while ( have_rows( 'repeater_field' ) ) : the_row(); ?>
						<tr>
							<td class="comparison"><?php the_sub_field( 'comparison_title' ); ?></td>
							<?php if ( get_sub_field( 'standard' ) == 'error' ) : ?>
								<td class="standard"><img src="<?php echo get_stylesheet_directory_uri(); ?>/img/error.png" alt="Error"></td>
							<?php elseif ( get_sub_field( 'standard' ) == 'success' ) : ?>
								<td class="standard"><img src="<?php echo get_stylesheet_directory_uri(); ?>/img/success.png" alt="Success"></td>
							<?php elseif ( get_sub_field( 'standard' ) == 'text' ) : ?>
								<td class="standard-text <?php the_sub_field( 'standard_color' ); ?>"><?php the_sub_field( 'standard_text' ); ?></td>
							<?php endif; ?>

							<?php if ( get_sub_field( 'm_series' ) == 'error' ) : ?>
								<td class="m-series"><img src="<?php echo get_stylesheet_directory_uri(); ?>/img/error.png" alt="Error"></td>
							<?php elseif ( get_sub_field( 'm_series' ) == 'success' ) : ?>
								<td class="m-series"><img src="<?php echo get_stylesheet_directory_uri(); ?>/img/success.png" alt="Success"></td>
							<?php elseif ( get_sub_field( 'm_series' ) == 'text' ) : ?>
								<td class="m-series-text <?php the_sub_field( 'm_series_color' ); ?>"><?php the_sub_field( 'm_series_text' ); ?></td>
							<?php endif; ?>

							<?php if ( get_sub_field( 'fm_series' ) == 'error' ) : ?>
								<td class="fm-series"><img src="<?php echo get_stylesheet_directory_uri(); ?>/img/error.png" alt="Error"></td>
							<?php elseif ( get_sub_field( 'fm_series' ) == 'success' ) : ?>
								<td class="fm-series"><img src="<?php echo get_stylesheet_directory_uri(); ?>/img/success.png" alt="Success"></td>
							<?php elseif ( get_sub_field( 'fm_series' ) == 'text' ) : ?>
								<td class="fm-series-text <?php the_sub_field( 'fm_series_color' ); ?>"><?php the_sub_field( 'fm_series_text' ); ?></td>
							<?php endif; ?>
						</tr>
					<?php endwhile; endif; ?>
				</tbody>
			</table>
		</div>
	</div>
</div>