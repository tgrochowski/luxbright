<?php
/**
 * Content-9
 * Buisness cases slider
 */ ?>
<?php $menu_item = get_sub_field( 'content_9_menu_item' ); ?>

<div id="<?php echo luxbright_create_section_id( $menu_item ); ?>" class="section content-9">
	<div class="row">
		<div class="medium-12 medium-centered columns">
			<?php if ( get_sub_field( 'title' ) ): ?>
				<h2 class="section-title"><?php the_sub_field( 'title' ); ?></h2>
			<?php endif; ?>
			<?php $business_cases = array( 
				'post_type' => 'business_cases', 
				'orderby' => 'asc', 
				'posts_per_page' => -1, 
				'paged' => $paged 
			); ?>

			<?php query_posts( $business_cases ); ?>
			<?php if ( have_posts() ) : ?>
				<div id="news-slider" class="flexslider news-slider">
					<ul class="slides">
						<?php while ( have_posts() ) : the_post(); ?>
							<li>
								<div class="content">
									<?php if ( get_field( 'type' ) == 'image' ) : ?>
										<div class="image">
											<div class="row">
												<div class="medium-6 columns">
													<?php $image = get_field( 'image' ); ?>
													<?php if ( !empty($image) ): 
														
														$alt = $image['alt'];
														$size = 'news_image';
														$thumb = $image['sizes'][ $size ];
														$width = $image['sizes'][ $size . '-width' ];
														$height = $image['sizes'][ $size . '-height' ]; ?>

														<img src="<?php echo $thumb; ?>" alt="<?php echo $alt; ?>" width="<?php echo $width; ?>" height="<?php echo $height; ?>" />
													<?php endif; ?>
												</div>
												<div class="large-5 large-offset-1 columns">
													<span class="category"><?php _e( 'Business cases', 'luxbright' ); ?></span>
													<h2 class="news-title"><?php the_title(); ?></h2>
													<?php the_excerpt(); ?>
													<a href="<?php echo site_url(); ?>/applications/#<?php the_ID(); ?>" class="read-more">Read more</a>
												</div>
											</div>
										</div>
									<?php elseif ( get_field( 'type' ) == 'video' ) : ?>
										<div class="image">
											<div class="row">
												<div class="medium-6 columns">
													<a data-open="content-9-modal">
														<?php $image = get_field( 'video_thumb' ); ?>
														<?php if ( !empty($image) ): 
															
															$alt = $image['alt'];
															$size = 'large';
															$thumb = $image['sizes'][ $size ];
															$width = $image['sizes'][ $size . '-width' ];
															$height = $image['sizes'][ $size . '-height' ]; ?>

															<img src="<?php echo $thumb; ?>" alt="<?php echo $alt; ?>" width="<?php echo $width; ?>" height="<?php echo $height; ?>" />
														<?php endif; ?>
													</a>
													<div class="full reveal" id="content-9-modal" data-reveal>
														<div class="flex-video">
															<?php the_field('video'); ?>
														</div>
														<button class="close-button" data-close aria-label="Close modal" type="button">
															<span aria-hidden="true">&times;</span>
														</button>
													</div>
												</div>
												<div class="large-5 large-offset-1 columns">
													<span class="category"><?php _e( 'Business cases', 'luxbright' ); ?></span>
													<h2 class="news-title"><?php the_title(); ?></h2>
													<?php the_excerpt(); ?>
													<a href="<?php echo site_url(); ?>/applications/#<?php the_ID(); ?>" class="read-more">Read more</a>
												</div>
											</div>
										</div>
									<?php else : ?>
										<div class="neutral">
											<div class="row text-center">
												<div class="large-12 columns">
													<span class="category"><?php _e( 'Business cases', 'luxbright' ); ?></span>
													<h2 class="news-title"><?php the_title(); ?></h2>
													<?php the_excerpt(); ?>
													<a href="<?php echo site_url(); ?>/applications/#<?php the_ID(); ?>" class="read-more">Read more</a>
												</div>
											</div>
										</div>
									<?php endif; ?>
								</div>
							</li>
						<?php endwhile; ?>
					</ul>
					<div class="clearfix"></div>
				</div>
			<?php endif; wp_reset_query(); ?>
		</div>
	</div>
</div>