<?php
/**
 * Content-13
 * Business cases section
 */ ?>
<?php $menu_item = get_sub_field( 'content_3_menu_item' ); ?>

<?php $business_cases = array( 
	'post_type' => 'business_cases', 
	'orderby' => 'asc', 
	'posts_per_page' => -1, 
	'paged' => $paged 
); ?>

<?php query_posts( $business_cases ); ?>
<?php if ( have_posts() ) : $i = 0; ?>
	<?php while ( have_posts() ) : the_post(); $i++; ?>
		<?php
		/**
		 * Add background color to every second section
		 */
		$count++;
		if ( $count % 2 == 0 )
			$section_class = "gray";
		else
			$section_class = "white";
		?>

		<?php if ( $count % 2 == 0 ) : ?>
			<div class="section content-13 <?php echo $section_class; ?>" id="<?php the_ID(); ?>">
				<div class="row">
					<div class="large-12 columns">
						<h2 class="section-title"><?php the_title(); ?></h2>
					</div>
				</div>
				<div class="row">
					<?php if ( get_field( 'type' ) == 'image' ) : ?>
						<div class="medium-4 large-4 columns">
							<?php $image = get_field( 'image' ); ?>
							<?php if ( !empty($image) ): 
								
								$alt = $image['alt'];
								$size = 'news_image';
								$thumb = $image['sizes'][ $size ];
								$width = $image['sizes'][ $size . '-width' ];
								$height = $image['sizes'][ $size . '-height' ]; ?>

								<img src="<?php echo $thumb; ?>" alt="<?php echo $alt; ?>" width="<?php echo $width; ?>" height="<?php echo $height; ?>" />
							<?php endif; ?>
						</div>
						<div class="medium-8 large-8 columns">
							<div class="text-columns">
								<?php the_content(); ?>
							</div>
						</div>
					<?php elseif ( get_field( 'type' ) == 'video' ) : ?>
						<div class="medium-4 large-4 columns">
							<a data-open="content-13-modal">
								<?php $image = get_field( 'video_thumb' ); ?>
								<?php if ( !empty($image) ): 
									
									$alt = $image['alt'];
									$size = 'large';
									$thumb = $image['sizes'][ $size ];
									$width = $image['sizes'][ $size . '-width' ];
									$height = $image['sizes'][ $size . '-height' ]; ?>

									<img src="<?php echo $thumb; ?>" alt="<?php echo $alt; ?>" width="<?php echo $width; ?>" height="<?php echo $height; ?>" />
								<?php endif; ?>
							</a>
						</div>
						<div class="full reveal" id="content-13-modal" data-reveal>
							<div class="flex-video">
								<?php the_field('video'); ?>
							</div>
							<button class="close-button" data-close aria-label="Close modal" type="button">
								<span aria-hidden="true">&times;</span>
							</button>
						</div>
						<div class="medium-8 large-8 columns">
							<div class="text-columns">
								<?php the_content(); ?>
							</div>
						</div>
					<?php else : ?>
						<div class="medium-8 medium-centered columns">
							<div class="text-columns">
								<?php the_content(); ?>
							</div>
						</div>
					<?php endif; ?>
				</div>
			</div>
		<?php else : ?>
			<div class="section content-13 <?php echo $section_class; ?>" id="<?php the_ID(); ?>">
				<div class="row">
					<div class="large-12 columns">
						<?php if ( $i == 1 ) : ?>
							<h1 class="page-title"><?php the_title(); ?></h1>
						<?php else : ?>
							<h2 class="section-title"><?php the_title(); ?></h2>
						<?php endif; ?>
					</div>
				</div>
				<div class="row">
					<?php if ( get_field( 'type' ) == 'image' ) : ?>
						<div class="medium-8 large-8 columns">
							<div class="text-columns">
								<?php the_content(); ?>
							</div>
						</div>
						<div class="medium-4 large-4 columns">
							<?php $image = get_field( 'image' ); ?>
							<?php if ( !empty($image) ): 
								
								$alt = $image['alt'];
								$size = 'news_image';
								$thumb = $image['sizes'][ $size ];
								$width = $image['sizes'][ $size . '-width' ];
								$height = $image['sizes'][ $size . '-height' ]; ?>

								<img src="<?php echo $thumb; ?>" alt="<?php echo $alt; ?>" width="<?php echo $width; ?>" height="<?php echo $height; ?>" />
							<?php endif; ?>
						</div>
					<?php elseif ( get_field( 'type' ) == 'video' ) : ?>
						<div class="medium-8 large-8 columns">
							<div class="text-columns">
								<?php the_content(); ?>
							</div>
						</div>
						<div class="medium-4 large-4 columns">
							<a data-open="content-13-modal">
								<?php $image = get_field( 'video_thumb' ); ?>
								<?php if ( !empty($image) ): 
									
									$alt = $image['alt'];
									$size = 'large';
									$thumb = $image['sizes'][ $size ];
									$width = $image['sizes'][ $size . '-width' ];
									$height = $image['sizes'][ $size . '-height' ]; ?>

									<img src="<?php echo $thumb; ?>" alt="<?php echo $alt; ?>" width="<?php echo $width; ?>" height="<?php echo $height; ?>" />
								<?php endif; ?>
							</a>
						</div>
						<div class="full reveal" id="content-13-modal" data-reveal>
							<div class="flex-video">
								<?php the_field('video'); ?>
							</div>
							<button class="close-button" data-close aria-label="Close modal" type="button">
								<span aria-hidden="true">&times;</span>
							</button>
						</div>
					<?php else : ?>
						<div class="medium-8 medium-centered columns">
							<div class="text-columns">
								<?php the_content(); ?>
							</div>
						</div>
					<?php endif; ?>
				</div>
			</div>
		<?php endif; ?>
	<?php endwhile; ?>
<?php endif; wp_reset_query(); ?>