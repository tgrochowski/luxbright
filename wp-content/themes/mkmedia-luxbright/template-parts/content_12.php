 <?php
/**
 * Content-12
 * Wide video section
 */ ?>
<?php $menu_item = get_sub_field( 'content_12_menu_item' ); ?>

<div id="<?php echo luxbright_create_section_id( $menu_item ); ?>" class="section content-12 parallax-window" data-z-index="2" data-parallax="scroll" data-image-src="<?php the_sub_field( 'background_image' ); ?>">
	<div class="row">
		<div class="medium-12 columns text-center">
			<?php if ( get_sub_field( 'title' ) ): ?>
				<h2 class="section-title"><?php the_sub_field( 'title' ); ?></h2>
			<?php endif; ?>
		</div>
	</div>
	<div class="row">
		<div class="medium-10 medium-centered columns text-center">
			<a data-open="content-12-modal">
				<img src="<?php echo get_stylesheet_directory_uri(); ?>/img/play.png" alt="Play" class="play-button" />
			</a>
			<div class="full reveal" id="content-12-modal" data-reveal>
				<div class="flex-video">
					<?php the_sub_field('video'); ?>
				</div>
				<button class="close-button" data-close aria-label="Close modal" type="button">
					<span aria-hidden="true">&times;</span>
				</button>
			</div>
		</div>
	</div>
</div>