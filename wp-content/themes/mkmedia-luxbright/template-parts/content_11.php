<?php
/**
 * Content-11
 * Map section
 */ ?>
<?php $menu_item = get_sub_field( 'content_11_menu_item' ); ?>

<div id="<?php echo luxbright_create_section_id( $menu_item ); ?>"  onclick="window.open('https://goo.gl/maps/5jf7wovDFAH2', '_blank')" style="cursor:pointer;">
	<div class="section content-11">
		<div class="row">
			<div class="large-12 columns">
				<?php if ( get_sub_field( 'title' ) ): ?>
					<h2 class="section-title"><?php the_sub_field( 'title' ); ?></h2>
				<?php endif; ?>
			</div>
		</div>
		<div class="row">
			<div class="medium-5 large-4 columns">
				<div class="box">
					<span>Phone number</span>
					<p>0771 000 00 00</p>
					<span>E-mail</span>
					<a href="mailto:info@luxbright.com">info@luxbright.com</a>
					<span>Visiting address</span>
					<p class="visiting-adress">Erik Dahlbergsgatan 11A <br>411 26 Göteborg<br>Sweden</p>
				</div>
			</div>
		</div>
	</div>
</div>