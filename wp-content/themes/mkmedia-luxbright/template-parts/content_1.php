<?php
/**
 * Content-1
 * Image and text section
 */ ?>
<?php $menu_item = get_sub_field( 'content_1_menu_item' ); ?>

<div id="<?php echo luxbright_create_section_id( $menu_item ); ?>" class="section content-1 <?php the_sub_field( 'remove_bottom_padding' ); ?>"<?php if ( get_sub_field( 'background_color' ) ): ?> style="background-color:<?php the_sub_field( 'background_color' ); ?>;"<?php endif; ?>>
	<div class="row">
		<?php if ( get_sub_field( 'top_title' ) ): ?>
			<h2 class="section-title text-center"><?php the_sub_field( 'top_title' ); ?></h2>
		<?php endif; ?>
		<?php if ( get_sub_field( 'image_on' ) == 'yes' ) : ?>
			<?php if ( get_sub_field( 'image_align' ) == 'left' ) : ?>
				<div class="medium-4 large-4 columns show-for-medium">
					<?php $image = get_sub_field( 'image' ); ?>
					<?php if ( !empty($image) ): 
						
						$alt = $image['alt'];
						$size = 'large';
						$thumb = $image['sizes'][ $size ];
						$width = $image['sizes'][ $size . '-width' ];
						$height = $image['sizes'][ $size . '-height' ]; ?>

						<img <?php if ( get_sub_field( 'remove_bottom_padding' ) == 'padding-off' ) : ?>class="position-bottom"<?php endif; ?> src="<?php echo $thumb; ?>" alt="<?php echo $alt; ?>" width="<?php echo $width; ?>" height="<?php echo $height; ?>" />
					<?php endif; ?>
				</div>
				<div class="medium-8 large-7 large-offset-1 columns text-selection-right<?php if ( get_sub_field( 'remove_bottom_padding' ) == 'padding-on' ) : ?> center-vertical<?php endif; ?>">
					<?php if ( get_sub_field( 'pre_title' ) ): ?>
						<p class="pre-title"><?php the_sub_field( 'pre_title' ); ?></p>
					<?php endif; ?>
					<?php if ( get_sub_field( 'title' ) ): ?>
						<h3 class="text-title"><?php the_sub_field( 'title' ); ?></h3>
					<?php endif; ?>
					<?php if ( get_sub_field( 'ingress_on' ) == 'yes' ) : ?>
						<p class="preamble"><?php the_sub_field( 'ingress' ); ?></p>
					<?php endif; ?>
					<?php the_sub_field( 'text' ); ?>
					<?php if ( get_sub_field( 'buttons_on' ) == 'yes' ) : ?>
						<a href="<?php the_sub_field( 'url' ); ?>" class="button<?php if ( get_sub_field( 'remove_bottom_padding' ) == 'padding-off' ) : ?> margin-bottom<?php endif; ?>"><?php the_sub_field( 'url_text' ); ?></a>
					<?php endif; ?>
				</div>
				<div class="small-12 columns show-for-small-only">
					<?php $image = get_sub_field( 'image' ); ?>
					<?php if ( !empty($image) ): 
						
						$alt = $image['alt'];
						$size = 'large';
						$thumb = $image['sizes'][ $size ];
						$width = $image['sizes'][ $size . '-width' ];
						$height = $image['sizes'][ $size . '-height' ]; ?>

						<img <?php if ( get_sub_field( 'remove_bottom_padding' ) == 'padding-off' ) : ?>class="position-bottom"<?php endif; ?> src="<?php echo $thumb; ?>" alt="<?php echo $alt; ?>" width="<?php echo $width; ?>" height="<?php echo $height; ?>" />
					<?php endif; ?>
				</div>
			<?php else : ?>
				<div class="medium-7 columns<?php if ( get_sub_field( 'remove_bottom_padding' ) == 'padding-on' ) : ?> center-vertical<?php endif; ?>">
					<?php if ( get_sub_field( 'pre_title' ) ): ?>
						<p class="pre-title"><?php the_sub_field( 'pre_title' ); ?></p>
					<?php endif; ?>
					<?php if ( get_sub_field( 'title' ) ): ?>
						<h3 class="text-title"><?php the_sub_field( 'title' ); ?></h3>
					<?php endif; ?>
					<?php if ( get_sub_field( 'ingress_on' ) == 'yes' ) : ?>
						<p class="preamble"><?php the_sub_field( 'ingress' ); ?></p>
					<?php endif; ?>
					<?php the_sub_field( 'text' ); ?>
					<?php if ( get_sub_field( 'buttons_on' ) == 'yes' ) : ?>
						<a href="<?php the_sub_field( 'url' ); ?>" class="button<?php if ( get_sub_field( 'remove_bottom_padding' ) == 'padding-off' ) : ?> margin-bottom<?php endif; ?>"><?php the_sub_field( 'url_text' ); ?></a>
					<?php endif; ?>
				</div>
				<div class="medium-4 large-offset-1 columns">
					<?php $image = get_sub_field( 'image' ); ?>
					<?php if ( !empty($image) ): 
						
						$alt = $image['alt'];
						$size = 'large';
						$thumb = $image['sizes'][ $size ];
						$width = $image['sizes'][ $size . '-width' ];
						$height = $image['sizes'][ $size . '-height' ]; ?>

						<img <?php if ( get_sub_field( 'remove_bottom_padding' ) == 'padding-off' ) : ?>class="position-bottom"<?php endif; ?> src="<?php echo $thumb; ?>" alt="<?php echo $alt; ?>" width="<?php echo $width; ?>" height="<?php echo $height; ?>" />
					<?php endif; ?>
				</div>
			<?php endif; ?>
		<?php else : ?>
			<div class="large-10 large-centered columns text-center">
				<?php if ( get_sub_field( 'pre_title' ) ): ?>
					<p class="pre-title"><?php the_sub_field( 'pre_title' ); ?></p>
				<?php endif; ?>
				<?php if ( get_sub_field( 'title' ) ): ?>
					<h3 class="text-title"><?php the_sub_field( 'title' ); ?></h3>
				<?php endif; ?>
				<?php if ( get_sub_field( 'ingress_on' ) == 'yes' ) : ?>
					<p class="preamble"><?php the_sub_field( 'ingress' ); ?></p>
				<?php endif; ?>
				<?php the_sub_field( 'text' ); ?>
				<?php if ( get_sub_field( 'buttons_on' ) == 'yes' ) : ?>
					<a href="<?php the_sub_field( 'url' ); ?>" class="button"><?php the_sub_field( 'url_text' ); ?></a>
				<?php endif; ?>
			</div>
		<?php endif; ?>
	</div>
</div>