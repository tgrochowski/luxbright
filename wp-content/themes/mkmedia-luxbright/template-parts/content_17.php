<?php
/**
 * Content-17
 * Place anchorpoints
 */ ?>
<?php $menu_items = luxbright_get_sub_menu_items(); ?>
<?php if ( ! empty ( $menu_items ) ) : ?>
	<div class="hash-container">
		<nav id="hash-navigation" class="sub-navigation hash-navigation">
			<ul class="inline-list">
				<?php foreach ( $menu_items as $menu_item ) : ?>
					<li><a href="#<?php echo luxbright_create_section_id( $menu_item ); ?>"><?php echo $menu_item; ?></a></li>
				<?php endforeach; ?>
			</ul>
		</nav>
	</div>
<?php endif; ?>