<?php
/**
 * Content-15
 * Slider section
 */ ?>
<?php $menu_item = get_sub_field( 'content_15_menu_item' ); ?>

<div id="<?php echo luxbright_create_section_id( $menu_item ); ?>" class="section content-15">
	<div class="section-upper">
		<div id="section-upper-inner" class="inner">
			<?php if ( get_sub_field( 'title' ) ): ?>
				<div class="row">
					<div class="large-12 columns">
						<?php if ( get_sub_field( 'title' ) ): ?>
							<h2 class="section-title"><?php the_sub_field( 'title' ); ?></h2>
						<?php endif; ?>
					</div>
				</div>
			<?php endif; ?>
			<?php if ( have_rows( 'repeater_field' ) ) : ?>
				<div class="row show-for-large-up">
					<div class="medium-12 columns">
						<div class="flexslider-controls">
							<ul class="working-control-nav">
								<?php while ( have_rows( 'repeater_field' ) ) : the_row(); ?>
									<li>
										<div class="box">
											<span class="image">
												<?php $image = get_sub_field( 'image' ); ?>
												<?php if ( !empty($image) ):

													$alt = $image['alt'];
													$size = 'slider_icon';
													$thumb = $image['sizes'][ $size ];
													$width = $image['sizes'][ $size . '-width' ];
													$height = $image['sizes'][ $size . '-height' ]; ?>

													<img src="<?php echo $thumb; ?>" alt="<?php echo $alt; ?>" width="<?php echo $width; ?>" height="<?php echo $height; ?>" />
												<?php endif; ?>
											</span>
											<span><?php the_sub_field( 'title' ); ?></span>
										</div>
									</li>
								<?php endwhile; ?>
							</ul>
						</div>
					</div>
				</div>
			<?php endif; wp_reset_postdata(); ?>
		</div>
	</div>
	<?php if ( have_rows( 'repeater_field' ) ) : ?>
		<div id="working-slider" class="working-slider">
			<ul class="slides">
				<?php while ( have_rows( 'repeater_field' ) ) : the_row(); ?>
					<li style="background-image: url(<?php the_sub_field( 'background_image' ); ?>);">
						<div class="row">
							<div class="medium-10 medium-centered large-8 columns">
								<div class="caption">
									<?php the_sub_field( 'text' ); ?>
									<?php if ( get_sub_field( 'buttons_on' ) == 'yes' ) : ?>
										<a href="<?php the_sub_field( 'url' ); ?>" class="button"><?php the_sub_field( 'url_text' ); ?></a>
									<?php endif; ?>
								</div>
							</div>
						</div>
					</li>
				<?php endwhile; ?>
			</ul>
		</div>
	<?php endif; wp_reset_postdata(); ?>
</div>
