<?php
/**
 * Content-18
 * Product animation
 */ ?>
<?php $menu_item = get_sub_field( 'content_18_menu_item' ); ?>

<div id="<?php echo luxbright_create_section_id( $menu_item ); ?>" class="section content-18">
	<div class="animation-video ov-hid">
		<div class="animation-video-wrapper">
			<video class="covervid-video show-for-large">
			    <source src="<?php echo get_stylesheet_directory_uri(); ?>/video/luxbright_tube_3.mp4" type="video/mp4">
			</video>
			
			<div class="video-content">
				<div class="big-title">
					<?php if ( get_sub_field( 'title' ) ): ?>
						<h2 class="section-title"><?php the_sub_field( 'title' ); ?></h2>
					<?php endif; ?>
				</div>
				<div class="boxes">
					<div class="box-1 foo">
						<h3><?php the_sub_field( 'box_1_title' ); ?></h3>
						<div class="divider"><span></span></div>
						<?php the_sub_field( 'box_1_text' ); ?>
					</div>
					<div class="box-2 foo">
						<h3><?php the_sub_field( 'box_2_title' ); ?></h3>
						<div class="divider"><span></span></div>
						<?php the_sub_field( 'box_2_text' ); ?>
					</div>
					<div class="box-3 foo">
						<h3><?php the_sub_field( 'box_3_title' ); ?></h3>
						<div class="divider"><span></span></div>
						<?php the_sub_field( 'box_3_text' ); ?>
					</div>
					<div class="box-4 foo">
						<h3><?php the_sub_field( 'box_4_title' ); ?></h3>
						<div class="divider"><span></span></div>
						<?php the_sub_field( 'box_4_text' ); ?>
					</div>
				</div>
			</div>
			<img src="<?php echo get_stylesheet_directory_uri(); ?>/img/product-startsida.png" class="show-for-small">
		</div>
	</div>
</div>