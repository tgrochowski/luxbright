<?php
/**
 * Grid
 * 2 rows with 3 columns
 */ ?>

<div class="section properties-grid ">
    <div class="row">
        <?php if ( $grid = get_sub_field( 'grid' ) ): ?>
        <?php foreach ($grid as $row) :?>
        <div class="property-box">
            <?php if ( !empty($row["img"]) ): ?>
            <img align="middle" class="property-img" src="<?=$row["img"]?>" />
            <?php endif; ?>
            
            <?php if ( !empty($row["title"]) ): ?>
                <h2 class="property-title text-center"><?=$row["title"]?></h2>
            <?php endif; ?>
                
            <?php if ( !empty($row["content"]) ): ?>
                <div class="property-content"><?=$row["content"]?></div>
            <?php endif; ?>
        </div>
        <?php endforeach; ?>
        <?php endif; ?>
               
    </div>
</div>