<?php
/**
 * Content-2
 * Two columns section
 */ ?>
<?php $menu_item = get_sub_field( 'content_2_menu_item' ); ?>

<?php if ( get_sub_field( 'type' ) == 'background_image' ) : ?>
	<div id="<?php echo luxbright_create_section_id( $menu_item ); ?>" class="section content-2" style="background:url(<?php the_sub_field( 'image' ); ?>) no-repeat center center; -webkit-background-size: cover; -moz-background-size: cover; -o-background-size: cover; background-size: cover;">
<?php elseif ( get_sub_field( 'type' ) == 'parallax' ) : ?>
	<div id="<?php echo luxbright_create_section_id( $menu_item ); ?>" class="section content-2 parallax-window" data-z-index="2" data-parallax="scroll" data-image-src="<?php the_sub_field( 'parallax_image' ); ?>">
<?php elseif ( get_sub_field( 'type' ) == 'background_color' ) : ?>
	<div id="<?php echo luxbright_create_section_id( $menu_item ); ?>" class="section content-2" style="background-color:<?php the_sub_field( 'color' ); ?>;">
<?php endif; ?>
	<div class="row">
            <div class="medium-12 columns text-center">
                <?php if ( get_sub_field( 'title' ) ): ?>
                    <h2 class="section-title"><?php the_sub_field( 'title' ); ?></h2>
                <?php endif; ?>
                <?php if ( get_sub_field( 'ingress' ) ): ?>
                    <div class="row">
                        <div class="medium-10 medium-centered columns text-center">
                            <p class="preamble"><?php the_sub_field( 'ingress', false, false ); ?></p>
                        </div>
                    </div>
                <?php endif; ?>
            </div>
	</div>
	<div class="row">
            <div class="medium-10 medium-centered columns text-center">
                <div class="row">
                    <div class="medium-12 columns font-big">
                        <?php if ( get_sub_field( 'left_text' ) ): ?>
                            <?php the_sub_field( 'left_text' ); ?>
                        <?php endif; ?>
                    </div>
                </div>
            </div>
	</div>
</div>