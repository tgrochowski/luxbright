<?php
/**
 * Content-14
 * Product split section
 */ ?>
<?php $menu_item = get_sub_field( 'content_14_menu_item' ); ?>

<div id="<?php echo luxbright_create_section_id( $menu_item ); ?>" class="section content-14">
	<div class="row">
		<div class="large-12 columns">
			<?php if ( get_sub_field( 'title' ) ): ?>
				<h2 class="section-title"><?php the_sub_field( 'title' ); ?></h2>
			<?php endif; ?>
		</div>
	</div>
	<div class="row">
		<div class="large-12 columns">
			<div id="split-slider" class="split-slider">
				<ul class="slides">
					<li>
						<div class="caption">
							<h3>First</h3>
							<p>Quisque id mi. Donec posuere vulputate arcu. Sed consequat, leo eget bibendum sodales, augue velit cursus nunc, quis gravida magna mi a libero. Vivamus laoreet. Nam eget dui.</p>
						</div>
					</li>
					<li>
						<div class="caption">
							<h3>Second</h3>
							<p>Quisque id mi. Donec posuere vulputate arcu. Sed consequat, leo eget bibendum sodales, augue velit cursus nunc, quis gravida magna mi a libero. Vivamus laoreet. Nam eget dui.</p>
						</div>
					</li>
					<li>
						<div class="caption">
							<h3>Third</h3>
							<p>Quisque id mi. Donec posuere vulputate arcu. Sed consequat, leo eget bibendum sodales, augue velit cursus nunc, quis gravida magna mi a libero. Vivamus laoreet. Nam eget dui.</p>
						</div>
					</li>
					<li>
						<div class="caption">
							<h3>Fourth</h3>
							<p>Quisque id mi. Donec posuere vulputate arcu. Sed consequat, leo eget bibendum sodales, augue velit cursus nunc, quis gravida magna mi a libero. Vivamus laoreet. Nam eget dui.</p>
						</div>
					</li>
					<li>
						<div class="caption">
							<h3>Fifth</h3>
							<p>Quisque id mi. Donec posuere vulputate arcu. Sed consequat, leo eget bibendum sodales, augue velit cursus nunc, quis gravida magna mi a libero. Vivamus laoreet. Nam eget dui.</p>
						</div>
					</li>
				</ul>
			</div>
			<div class="product-split" id="product-split">
				<img src="<?php echo get_stylesheet_directory_uri(); ?>/img/product.png" alt="Product" />
				<div class="flexslider-controls">
					<ul class="split-control-nav">
						<li class="first test"><span></span></li>
						<li class="second test"><span></span></li>
						<li class="third test"><span></span></li>
						<li class="fourth test"><span></span></li>
						<li class="fifth test"><span></span></li>
					</ul>
				</div>
			</div>
		</div>
	</div>
</div>