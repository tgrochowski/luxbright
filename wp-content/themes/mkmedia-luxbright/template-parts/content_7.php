<?php
/**
 * Content-7
 * Product page
 */ ?>
<?php $menu_item = get_sub_field( 'content_7_menu_item' ); ?>

<div id="<?php echo luxbright_create_section_id( $menu_item ); ?>" class="section content-7"<?php if ( get_sub_field( 'background_color' ) ): ?> style="background-color:<?php the_sub_field( 'background_color' ); ?>;"<?php endif; ?>>
	<?php if ( get_sub_field( 'title' ) ): ?>
		<div class="row">
			<div class="large-12 columns">
				<?php if ( get_sub_field( 'title' ) ): ?>
					<h2 class="section-title"><?php the_sub_field( 'title' ); ?></h2>
				<?php endif; ?>
			</div>
		</div>
	<?php endif; ?>
	<div class="row">
		<div class="small-3 small-centered medium-3 medium-uncentered large-3 large-offset-1 columns">
			<?php $image = get_sub_field( 'image' ); ?>
			<?php if ( !empty($image) ): 
				
				$alt = $image['alt'];
				$size = 'large';
				$thumb = $image['sizes'][ $size ];
				$width = $image['sizes'][ $size . '-width' ];
				$height = $image['sizes'][ $size . '-height' ]; ?>

				<img src="<?php echo $thumb; ?>" alt="<?php echo $alt; ?>" width="<?php echo $width; ?>" height="<?php echo $height; ?>" />
			<?php endif; ?>
		</div>
		<div class="small-12 medium-9 large-8 columns">
			<div class="content">
				<?php if ( get_sub_field( 'pre_title' ) ): ?>
					<p class="pre-title"><?php the_sub_field( 'pre_title' ); ?></p>
				<?php endif; ?>
				<?php if ( get_sub_field( 'desc_title' ) ): ?>
					<h3 class="desc-title"><?php the_sub_field( 'desc_title' ); ?></h3>
				<?php endif; ?>
				<?php the_sub_field( 'text' ); ?>
				<?php if ( get_sub_field( 'buttons_on' ) == 'yes' ) : ?>
					<a href="<?php the_sub_field( 'url' ); ?>" class="button"><?php the_sub_field( 'url_text' ); ?></a>
				<?php endif; ?>
			</div>

			<table>
				<thead>
					<tr>
						<th width="50">Type</th>
						<th width="150">Dwg.-No.</th>
						<th width="100">Radiation angles</th>
						<th width="100">Type of <br>X-ray tube</th>
						<th width="100">Fixation</th>
					</tr>
				</thead>
				<?php if ( have_rows( 'repeater_field_first' ) ) : while ( have_rows( 'repeater_field_first' ) ) : the_row(); ?>
					<tbody>
						<?php if ( have_rows( 'repeater_field' ) ) : while ( have_rows( 'repeater_field' ) ) : the_row(); ?>
							<tr>
								<?php if ( get_sub_field( 'new_type' ) == 'yes' ) : ?>
									<td class="type"><?php the_sub_field( 'type' ); ?></td>
								<?php else : ?>
									<td class="no-type"></td>
								<?php endif; ?>
								<?php if ( get_sub_field( 'file_on' ) == 'yes' ) : ?>
									<td class="file"><a href="<?php the_sub_field( 'url' ); ?>" target="_blank"><?php the_sub_field( 'url_text' ); ?></a></td>
								<?php else : ?>
									<td class="no-file"><?php the_sub_field( 'url_text' ); ?></td>
								<?php endif; ?>
								<td><?php the_sub_field( 'radiation_angles' ); ?>°</td>
								<td><?php the_sub_field( 'type_of_tube' ); ?></td>
								<td><?php the_sub_field( 'fixation' ); ?></td>
							</tr>
						<?php endwhile; endif; ?>
					</tbody>
				<?php endwhile; endif; ?>
			</table>
		</div>
	</div>
</div>