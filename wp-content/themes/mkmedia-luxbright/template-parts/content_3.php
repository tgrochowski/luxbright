<?php
/**
 * Content-3
 * Text and image with top title
 */ ?>
<?php $menu_item = get_sub_field( 'content_3_menu_item' ); ?>

<div id="<?php echo luxbright_create_section_id( $menu_item ); ?>" class="section content-3"<?php if ( get_sub_field( 'background_color' ) ): ?> style="background-color:<?php the_sub_field( 'background_color' ); ?>;"<?php endif; ?>>
	<div class="row">
		<div class="medium-10 medium-centered columns text-center">
			<?php if ( get_sub_field( 'title' ) ): ?>
				<h2 class="section-title"><?php the_sub_field( 'title' ); ?></h2>
			<?php endif; ?>
			<div class="row">
				<?php if ( get_sub_field( 'image_align' ) == 'left' ) : ?>
					<div class="medium-6 columns show-for-medium">
						<?php $image = get_sub_field( 'image' ); ?>
						<?php if ( !empty($image) ): 
							
							$alt = $image['alt'];
							$size = 'large';
							$thumb = $image['sizes'][ $size ];
							$width = $image['sizes'][ $size . '-width' ];
							$height = $image['sizes'][ $size . '-height' ]; ?>

							<img src="<?php echo $thumb; ?>" alt="<?php echo $alt; ?>" width="<?php echo $width; ?>" height="<?php echo $height; ?>" />
						<?php endif; ?>
					</div>
					<div class="medium-6 columns center-vertical">
						<?php the_sub_field( 'text' ); ?>
					</div>
					<div class="small-12 columns show-for-small-only">
						<?php $image = get_sub_field( 'image' ); ?>
						<?php if ( !empty($image) ): 
							
							$alt = $image['alt'];
							$size = 'large';
							$thumb = $image['sizes'][ $size ];
							$width = $image['sizes'][ $size . '-width' ];
							$height = $image['sizes'][ $size . '-height' ]; ?>

							<img src="<?php echo $thumb; ?>" alt="<?php echo $alt; ?>" width="<?php echo $width; ?>" height="<?php echo $height; ?>" />
						<?php endif; ?>
					</div>
				<?php else : ?>
					<div class="medium-6 columns center-vertical">
						<?php the_sub_field( 'text' ); ?>
					</div>
					<div class="medium-6 columns">
						<?php $image = get_sub_field( 'image' ); ?>
						<?php if ( !empty($image) ): 
							
							$alt = $image['alt'];
							$size = 'large';
							$thumb = $image['sizes'][ $size ];
							$width = $image['sizes'][ $size . '-width' ];
							$height = $image['sizes'][ $size . '-height' ]; ?>

							<img src="<?php echo $thumb; ?>" alt="<?php echo $alt; ?>" width="<?php echo $width; ?>" height="<?php echo $height; ?>" />
						<?php endif; ?>
					</div>
				<?php endif; ?>
			</div>
		</div>
	</div>
</div>