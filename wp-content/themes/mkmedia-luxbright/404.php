<?php get_header(); ?>

<div class="section error">
	<div class="row">
		<div class="large-12 columns">
			<div class="text-center">
				<h1 class="page-title"><span>404</span> <?php _e( 'Page could not be found', 'luxbright' ); ?></h1>
				<p><?php _e( 'Try the menu above or', 'luxbright' ); ?> <a href="<?php echo site_url(); ?>"><?php _e( 'go to the startpage', 'luxbright' ); ?></a>.</p>
			</div>
		</div>
	</div>
</div>

<?php get_footer(); ?>