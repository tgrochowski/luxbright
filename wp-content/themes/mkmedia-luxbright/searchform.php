<form method="get" class="searchform animated fadeIn" action="<?php echo esc_url( home_url( '/' ) ); ?>">
	<input type="text" class="field search-input" autocomplete="off" name="s" placeholder="<?php _e( 'Sök...', 'luxbright' ); ?>" />
</form>