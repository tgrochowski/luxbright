<!DOCTYPE html>
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7" lang="<?php echo substr(get_bloginfo('language'), 0, 2); ?>"> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8" lang="<?php echo substr(get_bloginfo('language'), 0, 2); ?>"> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9" lang="<?php echo substr(get_bloginfo('language'), 0, 2); ?>"> <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js" lang="<?php echo substr(get_bloginfo('language'), 0, 2); ?>"> <!--<![endif]-->
    <head>
        <meta charset="<?php bloginfo('charset'); ?>" />
        <title><?php bloginfo('name'); ?> | <?php is_front_page() ? bloginfo('description') : wp_title(''); ?></title>
        <meta http-equiv="x-ua-compatible" content="ie=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta name="description" content="<?php bloginfo('description'); ?>">
        <meta name="author" content="mkmedia.se">

        <link href="https://fonts.googleapis.com/css?family=Cantarell:400,400i,700,700i" rel="stylesheet">
        <link href="https://fonts.googleapis.com/css?family=Merriweather:300,300i,400,400i,700" rel="stylesheet">

        <!--[if IE]>
                <meta http-equiv='X-UA-Compatible' content='IE=edge,chrome=1'>
                <link rel="stylesheet" href="<?php echo get_stylesheet_directory_uri(); ?>/css/app-ie.css" />
        <![endif]-->
        <!--[if !IE]><!-->
        <link rel="stylesheet" href="<?php echo get_stylesheet_directory_uri(); ?>/css/app.css" />
        <!--<![endif]-->

        <link rel="shortcut icon" href="<?php echo get_template_directory_uri(); ?>/img/favicon.ico">
        <link rel="pingback" href="<?php bloginfo('pingback_url'); ?>">

        <?php wp_head(); ?>
        <script src="https://cdn.jsdelivr.net/scrollreveal.js/3.3.1/scrollreveal.min.js"></script>

        <!--[if lt IE 9]>
                <script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
        <![endif]-->
    </head>
    <body <?php body_class(implode(' ', luxbright_browser_body_class())); ?>>
        <div id="wrapper" class="wrapper">
            <div class="off-canvas-wrapper">
                <div class="off-canvas-wrapper-inner" data-off-canvas-wrapper>

                    <aside class="off-canvas position-right" id="offCanvasRight" data-off-canvas data-position="right">
                        <label><?php _e('Main navigation', 'luxbright'); ?></label>
                        <?php wp_nav_menu(array('theme_location' => 'primary', 'container' => false, 'menu_class' => 'off-canvas-list', 'walker' => new Off_Canvas_Walker())); ?>
                    </aside>

                    <div class="off-canvas-content" data-off-canvas-content>

                        <header id="masthead" class="site-header">
                            <div class="row">
                                <div class="large-12 columns">
                                    <div class="logotype">
                                        <a href="<?php echo home_url('/'); ?>" title="<?php echo esc_attr(get_bloginfo('name', 'display')); ?>" class="site-name" rel="home">
                                            <img src="<?php echo get_stylesheet_directory_uri(); ?>/img/logo.png" alt="<?php bloginfo('name'); ?>" width="167" height="39">
                                        </a>
                                    </div>
                                    <a class="right-off-canvas-toggle hide-for-large" href="#" data-open="offCanvasRight"><i class="fa fa-bars"></i></a>
                                </div>
                            </div>
                            <div class="row">
                                <div class="large-12 columns">
                                    <div class="wrapp">
                                        <nav id="main-navigation" class="site-navigation show-for-large">
                                            <?php wp_nav_menu(array('theme_location' => 'primary', 'container' => false, 'walker' => new Top_Bar_Walker())); ?>
                                        </nav>
                                    </div>
                                </div>
                            </div>
                        </header>

                        <div id="main" class="site-main" role="main">

                            <?php if (is_front_page()) : ?>
                                <div class="head-cover ov-hid">
                                    <div class="covervid-wrapper">
                                        <video class="covervid-video show-for-large" autoplay loop poster="<?php echo get_stylesheet_directory_uri(); ?>/img/top_image.jpg">
                                            <source src="<?php echo get_stylesheet_directory_uri(); ?>/video/luxbright_tube_slow.mp4" type="video/mp4">
                                        </video>
                                        <div class="video-content">
                                            <div class="inner-table">
                                                <div class="inner-cell">
                                                    <p class="first-row" id="first-row">Brilliant Micro Focus.</p>
                                                    <p class="middle-row">Ultra Fast Rise Time.</p>
                                                    <p class="bottom-row">Future of X-Ray imaging</p>
                                                    <?php if (is_active_sidebar('social_icons')) : ?>
                                                        <?php dynamic_sidebar('social_icons'); ?>
                                                    <?php endif; ?>

                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            <?php endif; ?>