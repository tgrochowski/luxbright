<?php get_header(); ?>

<div class="section single-event">
	<div class="row">
		<div class="medium-9 medium-centered columns">
			<?php if (have_posts()) : ?>
				<?php while (have_posts()) : the_post(); ?>

					<article id="event-<?php the_ID(); ?>">
						<h1 class="event-title"><?php the_title(); ?></h1>
						<div class="meta-info">
							<span class="date"><i class="fa fa-calendar-o"></i> <?php echo event_date(); ?></span> 
							<span class="location"><i class="fa fa-location-arrow"></i> <?php the_field('event_location'); ?></span>
						</div>
						<?php the_content(); ?>
					</article>

				<?php endwhile; ?>

				<a href="<?php echo site_url(); ?>/meet-us" class="button small radius"><span>Back to</span> Meet us</a>

			<?php endif; ?>
		</div>
	</div>
</div>

<?php get_footer(); ?>	