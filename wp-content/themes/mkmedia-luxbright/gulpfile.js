var gulp = require('gulp');
var $    = require('gulp-load-plugins')();
//var jshint = require('gulp-jshint');
var notify = require('gulp-notify');
var bourbon = require('node-bourbon');

// Browsers to target when prefixing CSS.
var COMPATIBILITY = ['last 2 versions', 'ie >= 9'];

// File paths to various assets are defined here.
var PATHS = {
  sass: [
    bourbon.includePaths,
    'bower_components/foundation-sites/scss',
    'bower_components/motion-ui/src/'
  ],
  javascript: [
    'bower_components/jquery/dist/jquery.js',
    'bower_components/parallax.js/parallax.min.js',
    'bower_components/what-input/what-input.js',
    'bower_components/isInViewport/lib/isInViewport.min.js',
    // 'bower_components/wow/dist/wow.min.js',
    // 'bower_components/waypoints/lib/noframework.waypoints.min.js',
    'bower_components/foundation-sites/js/foundation.core.js',
    'bower_components/foundation-sites/js/foundation.util.*.js',
    'bower_components/foundation-sites/js/foundation.*.js',
    'bower_components/flexslider/jquery.flexslider-min.js',
    'bower_components/jquery.cookie/jquery.cookie.js',
    'js/app.js',
    'js/map.js'
  ]
};

// Compile Sass into CSS
gulp.task('sass', function() {
  var minifycss = $.minifyCss();

  return gulp.src('./scss/app.scss')
    .pipe($.sass({
      includePaths: PATHS.sass
    })
      .on('error', $.sass.logError))
    .pipe($.autoprefixer({
      browsers: COMPATIBILITY
    }))
    .pipe(minifycss)
    .pipe(gulp.dest('./css'));
});

// Lint JavaScript
gulp.task('lint', function() {
  return gulp.src('./js/*.js')
    .pipe(jshint({
      curly: true,
      eqeqeq: true,
      eqnull: true,
      browser: true,
      globals: {
        jQuery: true
      }
    }))
    .pipe(jshint.reporter('default'))
    .pipe(notify(function (file) {
      if (file.jshint.success) {
        // Don't show something if success
        return false;
      }
      var errors = file.jshint.results.map(function (data) {
        if (data.error) {
          return "(" + data.error.line + ':' + data.error.character + ') ' + data.error.reason;
        }
      }).join("\n");
      return file.relative + " (" + file.jshint.results.length + " errors)\n" + errors;
    }));
});

// Combine JavaScript into one file and minify
gulp.task('javascript', function() {
  var uglify = $.uglify()
    .on('error', function (e) {
      console.log(e);
    });

  return gulp.src(PATHS.javascript)
    .pipe($.babel())
    .pipe($.concat('app.min.js'))
    .pipe(uglify)
    .pipe(gulp.dest('./js/dist'));
});

// Build the site and watch for file changes
gulp.task('default', ['sass', 'lint', 'javascript'], function() {
  gulp.watch(['./scss/**/*.scss'], ['sass']);
  gulp.watch(['./js/*.js'], ['lint']);
  gulp.watch(['./js/*.js'], ['javascript']);
});