<?php get_header(); ?>

<div class="search-top">
	<div class="row">
		<div class="large-12 columns">
        	<div class="search-box">
            	<?php get_search_form(); ?>
        	</div>
		</div>
	</div>
</div>

<div class="section search">
    <div class="row">
		<div class="medium-8 medium-centered columns">
			<div class="new-search">
		        <?php 
		        $allSearch = new WP_Query("s=$s&showposts=-1"); 
		        $key = esc_html($s, 1); 
		        $count = $allSearch->post_count; ?>
		        <h1><?php _e( 'Resultatet av', 'luxbright' ); ?> "<?php echo $key; ?>"</h1>
		        <p><?php echo $count; ?> <?php _e( 'inlägg/sidor funna', 'luxbright' ); ?></p>
		        <?php wp_reset_query(); ?>
		    </div>
		    <div class="search-posts">
				<?php if ( have_posts() ) : ?>
		   
		            <?php while ( have_posts() ) : the_post(); ?>
		                
		                <article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
		                	<h2><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h2>
	                        <?php the_excerpt(); ?>
		                </article>

		            <?php endwhile; ?>
		                
		            <?php foundation_pagination(); ?>
		                
		        <?php endif; ?>
	    	</div>
    	</div>
	</div>
</div>

<?php get_footer(); ?>