<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define('DB_NAME', 'luxbright');

/** MySQL database username */
define('DB_USER', 'root');

/** MySQL database password */
define('DB_PASSWORD', '123321');

/** MySQL hostname */
define('DB_HOST', 'localhost');

/** Database Charset to use in creating database tables. */
define('DB_CHARSET', 'utf8');

/** The Database Collate type. Don't change this if in doubt. */
define('DB_COLLATE', '');

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         '{ZkV($^#^X.0Ad*Qg]]Z{1vHv) Kz{VTjO4s[9P5<^t.-iCBV-6K|3q%]m 2<M`$');
define('SECURE_AUTH_KEY',  '<At7$D56,d:u&J?3E E_UiqzIUk;ho9j=}4gqN-C>B0mFRinVoF$Lr[Y[xHM*o;C');
define('LOGGED_IN_KEY',    'aAh325[:#3([mDL+{c|n>ui&),>wIpe~T)I-@)iDPfbcG:00uI&Nk D&I^_68TN&');
define('NONCE_KEY',        '&~^[6f:xGjon/Q|MS8i$LiQo==iB,.PBo=<pm#JR}H5c(VQ?Z]u.DriW0m;Wor-,');
define('AUTH_SALT',        'tAmI@+G_~#DI[mUO+sqPs zRWO!mfw*vrzH[=m ZDT!RKhZ|,RXMj;dFpEf5YI{>');
define('SECURE_AUTH_SALT', '<Ndp@lDa51X::A,RH-BpfqNKX7Dc//y@wczcAwYR>m@Sn4{69VCxNb5|z6.2+P_.');
define('LOGGED_IN_SALT',   'U iJ>hKuJP9@!lHYFr0>[;jo1Sft*Z0yFDQl1XLVUVoKY+7Q>m.<SiO&k[A@h*om');
define('NONCE_SALT',       'H$.aH|.Z|XbGZ#HfQJfPcm<~n;]AoFH?S<xcW978`~.{pUaTDE&BH(>owhq5`9yq');

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix  = 'lux_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the Codex.
 *
 * @link https://codex.wordpress.org/Debugging_in_WordPress
 */
define('WP_DEBUG', false);

/* That's all, stop editing! Happy blogging. */

/** Absolute path to the WordPress directory. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Sets up WordPress vars and included files. */
require_once(ABSPATH . 'wp-settings.php');
